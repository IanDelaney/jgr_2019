# a script wiht a bunch of nested for loops to grid search for different parameters.
# right now 4 parameters are included
using Distributed
if length(procs())==1
    addprocs()
end
@everywhere begin
    #using SUGSET, VAWTools, Interpolations, Parameters, Dierckx,
    using Pkg
    Pkg.activate(".")
    include("src/interface-real-glacier.jl")
    include("src/comparison_functions.jl")
end

@eval @everywhere begin
    lower=$lower
    upper=$upper
    RealGlacier=$RealGlacier
    pg=$pg
    SUGSET.thick(1,2,pg)
end

@everywhere begin

    const n_1 = 3# 11 #Dm_50
    const n_2 = 3#11 #fsl
    const n_3 = 3#11 #smoothin'
    const n_4 = 1 # access factor

    const v_1 = range(lower[1], stop=upper[1],length=n_1) # Dm_50
    const v_2 = range(lower[2], stop=upper[2],length=n_2)   # sliding fraction
    const v_3 = div.((range(lower[3],stop= upper[3],length=n_3))*2,2)  # smoothing
    const v_4 = 0.001 #[1e-5 1e-4 0.5e-3 1e-3 0.5e-2 1e-2 0.5e-1 1e-1 0.25 0.75]  # access

    @assert length(v_4)== n_4
end

# annual
model_abs_err =[]
model_err_sum = []
model_rank_cor =[]

function grid_search_hires(p, pg, pn, sedflux_rs, global_date, runoff_date_spin, runoff_date_mod, fp_save)
    # run many model runs
    out = pmap(nr->one_evaluation_hires(nr, pp, pg, pn, sedflux_rs, global_date,
                                         runoff_date_spin, runoff_date_mod), 1:(n_1*n_2*n_3*n_4);
           )
    out = hcat(out...)

    walltimes, vari_1, vari_2, vari_3, vari_4,
    err_02day, err_025day,  err_03day, err_04day, err_05day,  err_075day, err_day, err_125day,
    err_2day, err_250day, err_3day, err_5day, err_7day, nse_02day, nse_025day,  nse_03day, nse_04day,
    nse_05day,  nse_075day, nse_day, nse_125day, nse_2day, nse_250day, nse_3day, nse_5day, nse_7day, Qbs_sum, data_sum,runs =
        [out[i,:] for i=1:size(out,1)]

    # save to file
    headers= [ "wall-time" "err_02day" "err_025day"  "err_03day" "err_04day" "err_05day"  "err_075day" "err_day" "err_125day" "err_2day" "err_250day" "err_3day" "err_5day" "err_7day" "nse_02day" "nse_025day"  "nse_03day" "nse_04day" "nse_05day"  "nse_075day" "nse_day" "nse_125day" "nse_2day" "nse_250day" "nse_3day" "nse_5day" "nse_7day" "Qbs_sum" "data_sum" "Dm_50"  "fsl"  "smoothing" "access factor" "spin_runs"]
    a= [walltimes err_02day err_025day err_03day err_04day err_05day err_075day err_day err_125day err_2day err_250day err_3day err_5day err_7day nse_02day nse_025day nse_03day nse_04day nse_05day nse_075day nse_day nse_125day nse_2day nse_250day nse_3day nse_5day nse_7day Qbs_sum  data_sum vari_1 vari_2 vari_3 round.(vari_4,digits=5) runs]

    file = vcat(headers, a)
    writedlm("$(fp_save)/$(pg.name)_hi-res_results_mar18_access0001.txt", file,"\t")

    return [walltimes, vari_1, vari_2, vari_3, vari_4, err_02day, err_025day,  err_03day, err_04day, err_05day, err_075day,
            err_day, err_125day, err_2day, err_250day, err_3day, err_5day, err_7day, nse_02day, nse_025day,  nse_03day,
            nse_04day, nse_05day,  nse_075day, nse_day, nse_125day, nse_2day, nse_250day, nse_3day, nse_5day, nse_7day, Qbs_sum, data_sum, runs]
end

@everywhere function one_evaluation_hires(nr, pp, pg, pn, sedflux_rs, global_date,
                                           runoff_date_spin, runoff_date_mod)
    println("Running simulation $nr of $(n_1*n_2*n_3*n_4)")
    wt = time_ns()
    i,j,k,l = Tuple(CartesianIndices((n_1, n_2, n_3, n_4))[nr])

    ## redefine variables
    pp = Phys(pp,
              Dm = v_1[i],
              )
    pn = Num(pn,
             smooth_mobilization=v_4[l]
             )

    pg = RealGlacier(pg,
                     fsl = v_2[j],
                     Q_sm=Int(round(v_3[k])),




                     int_b_0_spin =  Spline1D(runoff_date_spin, pg.b_0_spin; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11),
                     int_b_0_sm_spin = Spline1D(runoff_date_spin,
                                                Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                                  pg.b_0_spin,
                                                                  (div(Int(round(v_3[k])),2)*2+1,)
                                                                  )
                                                ; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11
                                                ),
                     int_b_0    = Spline1D(runoff_date_mod,pg.b_0; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11),
                     int_b_0_sm =  Spline1D(runoff_date_mod,
                                            Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                              pg.b_0,
                                                              (div(Int(round(v_3[k])),2)*2+1,) # make odd
                                                              )
                                            ; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11
                                            )

                     )

#    println("\n Dm50:$(pp.Dm)  fsl:$(pg.fsl)  Q_sm:$(pg.Q_sm) \n")

    ## re-run the model with new variables
    err_02day, err_025day, err_03day, err_04day, err_05day, err_075day, err_day, err_125day,
    err_2day, err_250day, err_3day, err_5day, err_7day, nse_02day, nse_025day, nse_03day, nse_04day,
    nse_05day, nse_075day, nse_day, nse_125day, nse_2day, nse_250day, nse_3day, nse_5day, nse_7day, Qbs_sum, data_sum, runs  =
        comparerer(sedflux_rs,global_date,pg,pp,pn)


    ## save variable and result to file
    #    println("\n error: $(err_sum)")
    #     println("\n spearman: $(rank_cor)")

    return [(time_ns()-wt)/1e9, pp.Dm, pg.fsl, pg.Q_sm, pn.smooth_mobilization,
            err_02day, err_025day,  err_03day, err_04day, err_05day,  err_075day, err_day, err_125day,
            err_2day, err_250day, err_3day, err_5day, err_7day, nse_02day, nse_025day,  nse_03day, nse_04day,
            nse_05day,  nse_075day, nse_day, nse_125day, nse_2day, nse_250day, nse_3day, nse_5day, nse_7day, Qbs_sum, data_sum, runs ]
end

function grid_search_annual(pp, pg, pn, data, runoff_date_spin, runoff_date_mod, fp_save)
    # run many model runs
    out = pmap(nr->one_evaluation_annual(nr, pp, pg, pn, data,
                                         runoff_date_spin, runoff_date_mod), 1:(n_1*n_2*n_3*n_4);
           )
    out = hcat(out...)
    walltimes, vari_1, vari_2, vari_3, vari_4, model_abs_err, model_err_sum, model_rank_cor, runs = [out[i,:] for i=1:size(out,1)]

    # save to file
    headers= [    "wall-time"       "'error (sum)'" "'rank_cor(spearman)'" "'abs_err'"      "'Dm_50'"   "'fsl'"  "'smoothing'" "'access_fac'"   "spin_runs"]
    file = vcat(headers,[walltimes model_err_sum       model_rank_cor      model_abs_err     vari_1      vari_2     vari_3       round.(vari_4,digits=5)  runs])
    writedlm("$(fp_save)/$(pg.name)_annual_results_mar18_access0001.txt", file,"\t")


    return walltimes, vari_1, vari_2, vari_3, vari_4, model_abs_err, model_err_sum, model_rank_cor, runs
end

@everywhere function one_evaluation_annual(nr, pp, pg, pn, data,
                                           runoff_date_spin, runoff_date_mod)
    println("Running simulation $nr of $(n_1*n_2*n_3*n_4)")
    wt = time_ns()

    i,j,k,l = Tuple(CartesianIndices((n_1, n_2, n_3, n_4))[nr])


    ## redefine variables
    pp = Phys(pp,
              Dm = v_1[i],
              )
    pn= Num(pn,
            smooth_mobilization=v_4[l])

    pg = RealGlacier(pg,
                     fsl = v_2[j],
                     Q_sm=Int(round(v_3[k])),


                     int_b_0_spin =  Spline1D(runoff_date_spin, pg.b_0_spin; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11),
                     int_b_0_sm_spin = Spline1D(runoff_date_spin,
                                                Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                                  pg.b_0_spin,
                                                                  (div(Int(round(v_3[k])),2)*2+1,)
                                                                  )
                                                ; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11
                                                ),
                     int_b_0    = Spline1D(runoff_date_mod,pg.b_0; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11),
                     int_b_0_sm =  Spline1D(runoff_date_mod,
                                            Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                              pg.b_0,
                                                              (div(Int(round(v_3[k])),2)*2+1,) # make odd
                                                              )
                                            ; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11
                                            )
                     )

    println("\n Dm50:$(pp.Dm)  fsl:$(pg.fsl)  Q_sm:$(pg.Q_sm)  access: $(pn.smooth_mobilization) \n")

    ## re-run the model with new variables

    err_sum, rank_cor, abs_err, runs = comparerer_annual(data,pg,pp,pn)

    ## save variable and result to file
    println("\n error: $(err_sum)")
    println("\n spearman: $(rank_cor)")

    return [(time_ns()-wt)/1e9, pp.Dm, pg.fsl, pg.Q_sm, pn.smooth_mobilization, err_sum, rank_cor, abs_err, runs]
end
