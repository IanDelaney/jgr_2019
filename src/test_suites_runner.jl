# Bench-like synthetic run
using Dates
using Statistics
using Test
using Parameters
using PyPlot
import Roots: find_zero
using SUGSET
using SUGSET: erosion, post_proc, solvers
import SUGSET: zb, zs, source_water, width, source_till, thick, Valley, source_water_ddm, delta_h, day, year

include("save_run.jl")
include("comparison_functions.jl")



suites = ["temp_offset_bench"
          "vari_bench"
          ]

if homedir()=="/home/mauro"
    fp="/home/mauro/netfs/vaw/root"*fp
elseif homedir()== "/Users/ian"
    fp_save="/Users/ian/eth/subglacial_sed/modeling/test_cases/"
elseif homedir()== "/Users/idelaney"
    fp_save="/Users/idelaney/research/subglacial_sed/modeling/test_cases/"
elseif homedir()=="/home/delaneyi"
    fp_save = "/home/delaneyi/analysis/sed_modeling_outputs/test_cases/"
end

##################
### parameters ###
##################

# parameters to assert and
"Defines variables and parameters to check against"
@with_kw struct Check<:Real
    ht0 = zeros(299) .+ 0.0
    fsl = 2.5
    Dm  = 0.04
    source_average_time=day .* 1.5
    lr  = -0.0075
    fi  = 0.175
    till_lim = 1.0
    till_growth_lim = 0.75
    rs = 2650.0 	# dens sed
    rs_l = 1500.0 # density of loose sed

end

pc=Check()


 pp = Phys(DAMP=16, # annual variation set to SHMIP
          Dm=0.04,
          )
pn=Num()
println(pn.smooth_mobilization )

function vari_check(pg, pp, pn,pc)

    @test   mean(pc.ht0) ≈  mean(pg.ht0) atol=0.0001
    @assert length(pc.ht0) == length(pg.ht0)
    @assert pc.fsl == pg.fsl
    @assert pc.lr  ==  pp.lr
    @assert pc.Dm == pp.Dm
    @assert pc.fi  == pp.fi
    @assert pc.till_lim == pg.till_lim
    @assert pc.till_growth_lim == pg.till_growth_lim
    @assert pc.rs == pp.rs	# dens sed
    #    @assert pc.rs_l == pp.rs_l # density of loose sed
    @assert pc.source_average_time==pg.source_average_time

    println("vari_check passed!")
end

######################
### changing stuff ###
######################

temp_offset = [-4 -2 0 2 4]*1.0 #Suite C

vari = [2 1 0.5 0.25 0.1] #Suite B
B_0_term_test = [0 15*10 15*25 15*50 15*100] #Suite A

runs= 5

for i = 1:length(suites)

    for j = 1:runs

        run_name = "$(suites[i])_$(j)_testb"
        println(run_name)
        if suites[i] == "vari_bench"

            global pp = Phys(
                Dm=pc.Dm,
                DT=0.0,
                fi=pc.fi,
                ft=pc.fi,
                DDAMP= vari[j],
                DAMP = 16
            )
            global pg = Valley(tspan=(0,15*year),
                        source_average_time = pc.source_average_time,
                        fsl= pc.fsl,
                        ht0=pc.ht0)

        elseif suites[i] == "temp_offset_bench"

            global pp = Phys(pp,
                      DT  = temp_offset[j],
                      DAMP = 16,
                      fi=pc.fi,
                      ft=pc.fi,
                      )

            global pg = Valley(
                tspan=(0,15*year),
                source_average_time = pc.source_average_time,
                fsl= pc.fsl,
                ht0=pc.ht0)

        else
            error("wtf?!?!")
        end

        vari_check(pg, pp, pn, pc)

        println("starting run_model()")
        println(now())

        @time sol  = run_model(pg, pp, pn)
        println("run_model() finished \n\n")

        SUGSET.mass_con_check(sol,pg, pp, pn, tspan = (pg.tout[length(pg.tout)-length(pg.tout)÷10], pg.tout[end]), dt=0.1*day)
        save_run(sol,pg, pp, pn,run_name,fp_save)
    end
end
