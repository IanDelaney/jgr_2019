using JLD
using PyPlot
using Parameters
include("comparison_functions.jl")
using SUGSET
import SUGSET: year,day
using VAWTools
using JLD
using Dates
using Interpolations
clf()
fp = "/Users/ian/Desktop/esker_data_dump/"
#fp = "/Users/ian/eth/subglacial_sed/modeling/results/"
#fp = "/home/delaneyi/analysis/sed_modeling_outputs/"

fp = "/Users/idelaney/research/subglacial_sed/modeling/results/"

#year = 365*86400
glacier =["aletsch" "gorner" "gries"][2]
experiment = ["hi-res"  "annual"][1]
variables = ["hooke" "dm"][2]

#run_output=load("$(fp)$(glacier)_annual_0.15125_0.36_53.0.jld")# Gries good
#run_output=load("$(fp)$(glacier)_hi-res_0.03225_0.85_102.0.jld") # Aletsch good
#run_output=load("$(fp)$(glacier)_hi-res_0.062_1.725_102.0.jld")# Gorner good
#run_output=load("$(fp)gorner_hi-res_0.03225_1.2_5.0_0.0001.jld")
#run_output=load("$(fp)gorner_hi-res_0.09175_1.55_5.0_0.005.jld")
run_output=load("$(fp)gorner_hi-res_0.09175_1.725_5.0_0.01.jld")

zs     = run_output["zs"]
zb     = run_output["zb"]
width  = run_output["width"]
source_year  = run_output["source_year"]

if experiment == "hi-res"
    begin_sp = run_output["begin_sp"]
    end_sp   = run_output["end_sp"]
    sedflux_rs= run_output["sedflux_rs"]
end

tout   = run_output["tout"]
sgrid  = run_output["sgrid"]
ht0   = run_output["ht0"]
fsl   = run_output["fsl"]
Dm    = run_output["Dm"]

Qws =    run_output["Qws"]

Qws_sm = run_output["Qws_sm"]
Qbes  =  run_output["Qbes"]
hts =    run_output["hts"]
Qbs =    run_output["Qbs"]

# ###### plot it up #########
 fs = 25

# model_day, data_day = time_step_comp(abs.(Qbs[1,begin_sp:end_sp-1]),
#                                          abs.(sedflux_rs[1:end-1]),
#                                          86400,
#                                          tout[begin_sp:end_sp-1])

# model_Qw_day, data_Qw_day = time_step_comp(abs.(Qws[1,begin_sp:end_sp-1]),
#                                         abs.(sedflux_rs[1:end-1]),
#                                         86400,
#                                         tout[begin_sp:end_sp-1])

# println("NSE: $(NSE(model_day, data_day))")
# println("ERR: $(abs_error(model_day, data_day)*86400)")

# figure("time",figsize=(15,15/1.6))
# ax=subplot(2,1,1)#; hold(true)
# plot(unix2datetime.(tout[begin_sp:end_sp]), -Qws[1,begin_sp:end_sp], color="black", linewidth="1.5")
# #plot(tout[begin_sp:end_sp]/year+1970, -Qws_sm[1,begin_sp:end_sp], linewidth="0.5")
# ylabel(L"$\mathrm{Q_w}$ at snout $(\mathrm{m^3s^{-1}})$",fontsize=fs)
# ax.locator_params(axis ="x", nbins=4)

# ylim([0,maximum(-Qws[1,begin_sp:end_sp])])
# title("$(glacier)gletscher",fontsize=fs*1.5)
# tick_params(axis="both" ,labelsize=fs)

# subplot(2,1,2, sharex=ax)
# #plot(global_date/year+1970, sedflux_proper)
# plot(unix2datetime.(range(tout[begin_sp],stop=tout[end_sp],length=length(model_day))), data_day, linewidth="1.75")
# plot(unix2datetime.(range(tout[begin_sp],stop=tout[end_sp],length=length(model_day))), model_day, linewidth="2")
# if glacier == "aletsch"
#     ylim([0, 0.065])
# else
#     ylim([0,max(maximum(data_day),maximum(model_day))])
# end
# ylabel(L"$\mathrm{Q_s}$ at snout $(\mathrm{m^3s^{-1}})$",fontsize=fs)
# ax.locator_params(axis ="y", nbins=4)
# legend(["data", "model output"],fontsize=fs)
# tick_params(axis="both" ,labelsize=fs)
# tight_layout()

# smid=zeros(length(sgrid)-1)

# for i =length(sgrid)-1:-1:1
#     smid[i] = (sgrid[i+1] + sgrid[i])/2 #non stagard grid
# end

#  savefig("/Users/idelaney/research/subglacial_sed/sed_erosion_paper/figs/$(glacier)_$(experiment)_time_series.pdf")


# if glacier == "aletsch"
#     limer = 0.052
# else
#     limer = 0.008
# end

# # cmap=ColorMap("YlGn")
# # colors = cmap(linspace(1,0,length(model_day)))

# figure("scatter",figsize=(7,7))
# ax=subplots_adjust(hspace=0)
# scat = scatter(data_day, model_day,s=30, c="gray", edgecolor="None",zorder=0)
# plot(data_day,data_day , color = "black", linewidth=1,zorder=1)
# ylim([0,limer])
# xlim([0,limer])
# tick_params(axis="both" ,labelsize=fs)
# xlabel(L"data (Q$_{\mathrm{s}}$, m$^{3}$ s$^{-1}$)",fontsize=fs)
# ylabel(L"model (Q$_{\mathrm{s}}$, m$^{3}$ s$^{-1}$)",fontsize=fs)
# title("$(glacier)gletscher",fontsize=fs*1.25)
# tight_layout()
# savefig("/Users/idelaney/research/subglacial_sed/sed_erosion_paper/figs/$(glacier)_$(experiment)_scatter.pdf")

# figure("cumu",figsize=(7,7))
# ax=subplots_adjust(hspace=0)

# plot(cumsum(model_Qw_day[1:div(length(model_Qw_day),2)]*0.5*86400)./maximum(cumsum(model_Qw_day[1:div(length(model_Qw_day),2)]*0.5*86400)),
#      cumsum(model_day[1:div(length(model_Qw_day),2)]*0.5*86400)./maximum(cumsum(model_day[1:div(length(model_Qw_day),2)]*0.5*86400))
#      ,linewidth=2, color="black")

# plot([0,cumsum(model_Qw_day[1:div(length(model_Qw_day),2)])[end]*0.5*86400]./maximum(cumsum(model_Qw_day[1:div(length(model_Qw_day),2)]*0.5*86400)),
#      [0, cumsum(model_day[1:div(length(model_Qw_day),2)])[end]*0.5*86400]./maximum(cumsum(model_day[1:div(length(model_Qw_day),2)]*0.5*86400)) , color = "black", linewidth=0.75, zorder=1)


# plot(cumsum(model_Qw_day[div(length(model_Qw_day),2):end]*0.5*86400)./maximum(cumsum(model_Qw_day[div(length(model_Qw_day),2):end]*0.5*86400)),
#      cumsum(model_day[div(length(model_Qw_day),2):end]*0.5*86400) ./  maximum(cumsum(model_day[div(length(model_Qw_day),2):end]*0.5*86400)), #-sum(model_Qw_day[1,div(length(model_Qw_day),2)]*0.5*86400),
#      linewidth=2, color="grey")


# plot([0,cumsum(model_Qw_day[div(length(model_Qw_day),2):end])[end]*0.5*86400]./maximum(cumsum(model_Qw_day[div(length(model_Qw_day),2):end])*0.5*86400),
# [0, cumsum(model_day[div(length(model_Qw_day),2):end])[end]*0.5*86400]./maximum(cumsum(model_day[div(length(model_day),2):end])*0.5*86400), color = "grey", linewidth=0.75, zorder=1)

# tick_params(axis="both" ,labelsize=fs)
# xlabel(L"cumul. Q$_{\mathrm{w}}$ (m$^{3}$ s$^{-1}$)",fontsize=fs)
# ylabel(L"cumul. Q$_{\mathrm{s}}$ (m$^{3}$ s$^{-1}$)",fontsize=fs)
# title("$(glacier)gletscher",fontsize=fs*1.25)
# tight_layout()

#  savefig("/Users/idelaney/research/subglacial_sed/sed_erosion_paper/figs/$(glacier)_$(experiment)_mass-mass.pdf")

# ##############


### discharge stuff
# Qbs_avg = zeros(length(sgrid))
# Qbes_avg = zeros(length(sgrid))
# Qws_avg = zeros(length(sgrid))

# for j =1:length(sgrid)
#    Qbs_avg[j] =  mean(Qbs[j,end-450:end-150])
#     Qbes_avg[j] =  mean(Qbes[j,end-450:end-150])
#     Qws_avg[j] =  mean(Qws[j,end-450:end-150])
# end
# #Qws_avg[end] = 1e-4

 figure("space", figsize=(4*4,4))

# exag = abs.(zs-zb)*1.5

# ax=subplot(3,1,1)
# subplots_adjust(hspace=0.0)
# plot(sgrid, zb + exag, linewidth=1,color="blue")
# fill_between(sgrid, zb + exag,0,color="skyblue")
# plot(sgrid, zb, linewidth=1, color="black")
# fill_between(sgrid,zb,color="black")
# xlim([0,maximum(sgrid)])
# ylim([minimum(zb),maximum(zs)])
# ylabel(L"z ($\mathrm{m}$)",fontsize=fs)
# #axvline(x=sgrid[cell]
# #title("$(glacier)gletscher",fontsize=fs*1.5)
# tick_params(axis="y" ,labelsize=fs)
# setp(ax.get_xticklabels(),visible=false) # Disable x tick labels

# ax2 = ax.twinx()
# ax2.spines["right"]
# plot(smid, width, color="grey", linewidth=2)
# ylabel("w (m)",fontsize=fs)
# ylim([0, maximum(width)])
# tick_params(axis="y" ,labelsize=fs)
# setp(ax.get_xticklabels(),visible=false) # Disable x tick labels
# subplots_adjust(hspace=0.0)

### till height
#axb=subplot(3,1,2, sharex=ax)

hts_= hts.u[1,end]
for i = 1:length(ht0)
    if hts.u[1,end][i] < 1e-4
        hts_[i] = 1e-4
    end
end
ht0_ = ht0[:,end]
for i = 1:length(ht0)
    if ht0[i,end] < 1e-4
        ht0_[i] = 1e-4
    end
end

semilogy(sgrid[1:end-1] .+ 0.5 * (sgrid[1] - sgrid[2]), hts_,linewidth=2,color="black", label =L"$\mathrm{H_{t_{0}}}$")
fill_between(sgrid[1:end-1] .+ 0.5*(sgrid[1]-sgrid[2]), hts_,1e-4,color="indianred")
semilogy(sgrid[1:end-1] .+ 0.5*(sgrid[1]-sgrid[2]), ht0_, color="black", label =L"$\mathrm{H_{t_{end}}}$", "-.")


# plot(sgrid[1:end-1] .+0.5 *(sgrid[1]-sgrid[2]), source_year, color="grey", label ="yearly source")

ylim([1e-4,1.1])
xlim([0,maximum(sgrid)])
xlabel(L"x ($\mathrm{m}$)",fontsize=fs)
ylabel(L"$\mathrm{H_t}$ ($\mathrm{m}$)",fontsize=fs)
tick_params(axis="both" ,labelsize=fs)
#setp(axb.get_xticklabels(),visible=false) # Disable x tick labels
#legend(fontsize=fs)
title("Δσ:0.01 (m)",fontsize=fs)
subplots_adjust(hspace=0.0)
savefig("/Users/idelaney/research/subglacial_sed/sed_erosion_paper/figs/$(glacier)_$(experiment)_001profile.pdf")

# axc = axb.twinx()
# axc.spines["right"]
# plot(sgrid, -Qbes_avg, color="blue", linewidth="2", label=L"$\mathrm{\tilde{Q}_{sc}}$")
# ylabel(L"$\mathrm{Q_{sc}}$ $(\mathrm{m^3s^{-1}})$", fontsize=fs, color="blue")
# tick_params(axis="both" ,labelsize=fs)
# #PyPlot.ylim([1e-4,maximum(-Qbes_avg)])
# legend(fontsize=fs)


# # sediment
# ax3=subplot(3,1,3, sharex=ax)
# plot(sgrid, -Qbs_avg,  color="red", linewidth="2", label=L"$\mathrm{\tilde{Q}_s}$")
# ylabel(L"$\mathrm{Q_s}$  $(\mathrm{m^3s^{-1}})$", fontsize=fs,color="red")
# tick_params(axis="both" ,labelsize=fs)
# PyPlot.ylim([0,maximum(-Qbs_avg)*1.2])
# PyPlot.xlim([0,maximum(sgrid)])
# xlabel(L"x ($\mathrm{m}$)",fontsize=fs)
# legend(fontsize=fs)
# subplots_adjust(hspace=0.0)

# ax4 = ax3.twinx()
# ax4.spines["right"]
# plot(sgrid, -Qws_avg, color="black", linewidth="2", label=L"$\mathrm{\tilde{Q}_w}$")
# ylabel(L"$\mathrm{Q_w}$ $(\mathrm{m^3s^{-1}})$",fontsize=fs)
# tick_params(axis="both" ,labelsize=fs)
# PyPlot.ylim([0,maximum(-Qws_avg)])
# legend(fontsize=fs)
# tight_layout()
# subplots_adjust(hspace=0.0)

# savefig("/Users/idelaney/research/subglacial_sed/sed_erosion_paper/figs/$(glacier)_$(experiment)_profile.pdf")
