#################################
### create glacier parameters ###
#################################
using SUGSET, Parameters, Dierckx, Interpolations, Images
using SUGSET: zs, zb, erosion

const InterpType = Interpolations.GriddedInterpolation{Float64,1,Float64,Interpolations.Gridded{Linear},Tuple{Array{Float64,1}}}
const SgridType  = StepRangeLen{Float64,Base.TwicePrecision{Float64},Base.TwicePrecision{Float64}}

"Defines Real glacier"
@with_kw struct RealGlacier <: Glacier @deftype Float64
    name::String # glacier name
   # The spatial domain:
    domain::Tuple{Float64, Float64}=(minimum(x_surf), maximum(x_surf))

    # the finite volume grid (the boundaries not cell-centers):
    sgrid::SgridType = range(domain[1],stop=domain[2],length=425)

    # Time domai
    tspan::Tuple{Float64, Float64}
    tspan_spin::Tuple{Float64, Float64}
    tspan_run::Tuple{Float64, Float64}
    tout::Vector{Float64}=tspan[1]:.2*SUGSET.day:tspan[2] # output times
    #@assert tout[1]>=tspan[1]
    #@assert tout[2]<=tspan[2]

    # IC & BC
    ht0::Vector{Float64}=sgrid[1:end-1]*0 .+ .75 # initial till layer thickness
    Q_top = 0 # water discharge entering the domain at top
    Qb_top = 0 # bedload entering the domain at top

    # Topo
    para = 0.05

    #till
    till_growth = 0.001/SUGSET.year # till grown (for steady growth parameterizations)
    fsl = 0.8 # fraction of basal sliding
    till_growth_lim = 0.75 # maximium amount of till, above which till stops being produced
    till_lim = 1.0 # maximium amount of till, above which is it passed to the next cell
    @assert till_growth_lim < till_lim

    # misc parameters
    float_frac = 0.99 # 1<=>overburden pressure, 0<=>atmospheric
    @assert 0<=float_frac<1

    #hydrology
    source_average_time =10
    source_quantile =0.75
    gamma=-0.00625/SUGSET.year #mass balance gradient

    Q_sm = 41 #smoothing factor for discharge  maw: this is awkward with units as changing the sampling rate will change this.  Better use window in units of time

    hydro_type::String=["real","ddm"][1]

    spin::Bool = false # helps parameterize spin up functions... default is false
    spin_fac = 1



    # mass balance at terminus
    b_0::Array{Float64} = boxcar(runoff_b0_sort!(b_0_read_in)[1],3)
    b_0_sm::Array{Float64} = zeros(length(b_0_read_in))
    b_0_spin::Array{Float64} = boxcar(runoff_b0_sort!(b_0_spin_read_in)[1],3)
    b_0_sm_spin::Array{Float64} = zeros(length(b_0_read_in))


    ####
    int_b_0::Dierckx.Spline1D = Spline1D(runoff_date_mod,b_0; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11)
    int_b_0_sm::Dierckx.Spline1D = Spline1D(runoff_date_mod,
                                            Images.mapwindow( v->quantile(v, source_quantile),
                                                              b_0,
                                                              (Int(div(Q_sm,2)*2+1),) # make odd
                                                              )
                                            ; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11
                                            )

    int_b_0_spin::Dierckx.Spline1D    = Spline1D(runoff_date_spin,b_0_spin; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11)
    int_b_0_sm_spin::Dierckx.Spline1D = Spline1D(runoff_date_spin,
                                                 Images.mapwindow( v->quantile(v, source_quantile),
                                                                   b_0_spin,
                                                                   (Int(div(Q_sm,2)*2+1),)
                                                                   )
                                                 ; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11
                                                 )
    ####
    # Topo
    spl_surf::Dierckx.Spline1D=spl_surf
    spl_bed::Dierckx.Spline1D=spl_bed
    int_width::InterpType=int_width

end

#########################
### shape data
#########################

"""
makes surface
"""
SUGSET.zs(s,t, pg::RealGlacier) = Dierckx.evaluate(pg.spl_surf,s) # get surface from interpolation

"""
makes bed
"""
SUGSET.zb(s,t, pg::RealGlacier) = Dierckx.evaluate(pg.spl_bed,s)# get bed from interpolations

SUGSET.width(s,t, pg::RealGlacier) = pg.int_width(s)
SUGSET.thick(s,t, pg::RealGlacier) = zs(s,t,pg)-zb(s,t,pg)

SUGSET.gradzs(s,t,pg::RealGlacier) = Dierckx.derivative(pg.spl_surf, s)#; nu=1)
SUGSET.gradzb(s,t,pg::RealGlacier) = Dierckx.derivative(pg.spl_bed, s)#;  nu=1)


"""
source_water_meas_Q_sm(s,t,pg,pp,pd)
smoothed souce of water for a given cell
"""
function source_water_meas_Q_sm(s,t,pg,pp)

    if pg.spin == true
        out = max.(0, evaluate(pg.int_b_0_sm_spin, t) .+ pg.gamma .*(zs(s,0,pg)-zs(pg.sgrid[1],0,pg)))
    elseif  pg.spin == false
        out = max.(0, evaluate(pg.int_b_0_sm, t) .+ pg.gamma .*(zs(s,0,pg)-zs(pg.sgrid[1],0,pg)))

    else
        error("spin or not!!!!")
    end
end

"""
source_water_meas_Q(s,t,pg,pp,pd)
souce of water for a given cell
"""
function source_water_meas_Q(s,t,pg,pp)
    if pg.spin == true
        out = max.(0, evaluate(pg.int_b_0_spin,t) .+ pg.gamma .*(zs(s,0,pg)-zs(pg.sgrid[1],0,pg)))
    elseif pg.spin == false
        out = max.(0, evaluate(pg.int_b_0,t)    .+ pg.gamma .*(zs(s,0,pg)-zs(pg.sgrid[1],0,pg)))
    else
        error("spin or not!!!!")
    end
end

#########################
### water and till sources
#########################

function SUGSET.source_water_sm(s,t,pg::RealGlacier,pp)
    source_water_meas_Q_sm(s,t,pg,pp) # smoothed discharge for channel size parameterization
end

function SUGSET.source_water(s,t,pg::RealGlacier,pp)
    if pg.hydro_type=="real"
        source_water_meas_Q(s,t,pg,pp)  # actual discharge for determining water velocity
    elseif pg.hydro_type=="ddm"
        SUGSET.source_water_ddm(s,t,pg,pp)
    else
        error
    end
end
