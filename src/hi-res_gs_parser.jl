# script to put together grid search for annual runs
using PyPlot
using DelimitedFiles
glacier = ["gorner" "aletsch"][1]
variables = ["hooke" "dm"][1]
save = [true false][2]
plotting = [true false][2]

grid_search = readdlm("/Users/idelaney/research/subglacial_sed/modeling/results/gridsearch/$(glacier)_hi-res_results_mar18_access0001.txt",'\t', Float64,skipstart=1)

fp_save = "/Users/idelaney/research/subglacial_sed/sed_erosion_paper/figs/"

if glacier == "aletsch"
    NSE_emp = [0.19338606093782884
               0.19516668035702478
               0.1976278299560299
               0.20375438152367253
               0.21295042046870472
               0.24364885003027137
               0.2685674037781549
               0.3113241883227975
               0.347813759063337
               0.38754593438999485
               0.41384909819656623
               0.5023637525698588
               0.5245570302427068
               ]
    ERR_emp = [ 143270.09853912308
                141632.457243534
                140073.31885093157
                136635.50856986936
                133707.37931184788
                129351.29978240043
                124784.51952551317
                121136.91115113097
                115036.70213540088
                108941.06984816787
                103599.
                89048.37804630623
                78066.8690908472
               ]


else
    NSE_emp=[ 0.5066579820343827
              0.5123365182922505
              0.514963939794831
              0.5221887772243818
              0.5307522288303508
              0.5605809047485466
              0.5565578874341914
              0.5635594639326884
              0.5851430171111034
              0.5733728313262043
              0.5975689802524504
              0.6262969451691727
              0.62811760365201
             ]

    ERR_emp=[24309.233129192446
             23775.49603659627
             23433.12731960506
             22889.94139695814
             22556.115261655235
             22056.876722504057
             21347.212397628307
             21335.297917830027
             19870.271495301564
             19420.250270141758
             18570.214394685034
             16088.016907571342
             15493.209207186874
             ]
end

        # 1     2    3    4    5      6     7     8    9     10    11     12    13
        #4.8   6.0  7.2  9.6  12.0  18.0  24.0  30.0  48.0  60.0  72.0  120.0  168.0
times = [0.2   0.25 0.3  0.4  0.5   0.75   1    1.25   2     2.5   3     5       7]*86400

index_of_times = 7
if glacier == "gorner"
    base_cor = 0.5
else
    base_cor = 0.5
end

wall_time =  grid_search[:,1]
err_02day = grid_search[:,2]
err_025day = grid_search[:,3]
err_03day = grid_search[:,4]
err_04day = grid_search[:,5]
err_05day = grid_search[:,6]
err_075day = grid_search[:,7]
err_day = grid_search[:,8]
err_125day = grid_search[:,9]
err_2day = grid_search[:,10]
err_250day = grid_search[:,11]
err_3day = grid_search[:,12]
err_5day = grid_search[:,13]
err_7day = grid_search[:,14]

error = grid_search[:,2:14]

nse_02day = grid_search[:,15]
nse_025day = grid_search[:,16]
nse_03day = grid_search[:,17]
nse_04day = grid_search[:,18]
nse_05day = grid_search[:,19]
nse_075day = grid_search[:,20]
nse_day = grid_search[:,21]
nse_125day = grid_search[:,22]
nse_2day = grid_search[:,23]
nse_250day = grid_search[:,24]
nse_3day = grid_search[:,25]
nse_5day = grid_search[:,26]
nse_7day = grid_search[:,27]

nse = grid_search[:,15:27]

Qbs_sum = grid_search[:,28]
data_sum = grid_search[:,29]

Dm_50 = grid_search[:,30]
fsl = grid_search[:,31]
smoothing = grid_search[:,32]
hooke = grid_search[:,33]

paras = grid_search[:,20:32]

spins = grid_search[:,34]

##########################
### pull out best runs ###
##########################
"""
        return best model run for a time_step by variable 1
        note nse should be mulitplied by neg 1 for best answer
"""
function return_best(var, dm_50, fsl, smoothing, hooke)

    data = [var  dm_50  fsl  smoothing hooke]

    sorted = sortslices(data,dims=1, by = x->x[1])

    return sorted[1,:]
end


fs=12
ms=40

#############################
### make the cut and sort ###
#############################


function cut_and_sort(error, nse, Qbs, data, times, index_of_times, dm_50, fsl, smoothing, hooke, base_cor)

    stdy_pd_err = []
    nse_eff = []
    abs_err = []
    Dm_50 = []
    FSL = []
    qsm = []
    hooke_ =[]

    for i = 1:length(fsl)
        if nse[i,index_of_times] >= base_cor

            push!(stdy_pd_err, abs(Qbs_sum[i]-data_sum[i])*times[index_of_times])
            push!(nse_eff,     nse[i,index_of_times])
            push!(abs_err,     error[i,index_of_times]*times[index_of_times])
            push!(Dm_50,       dm_50[i])
            push!(FSL,         fsl[i])
            push!(qsm,         smoothing[i])
            push!(hooke_,      hooke[i])

        end
    end

    return [stdy_pd_err nse_eff abs_err Dm_50 FSL qsm hooke_]
end


error_b_ts = zeros(length(times),5)
tot_error_b_ts = [0 0 0 0 0]
nse_b_ts = zeros(length(times),5)

sorted_and_cut =[]

for i = 1:length(times)

    sorted_and_cut = cut_and_sort(error, nse, Qbs_sum, data_sum, times, i, Dm_50, fsl, smoothing, hooke, base_cor)

    error_b_ts[i,:] = return_best(sorted_and_cut[:,3],sorted_and_cut[:,end-3],sorted_and_cut[:,end-2],sorted_and_cut[:,end-1], sorted_and_cut[:,end])  #

    nse_b_ts[i,:]   = return_best(1 .- sorted_and_cut[:,2],sorted_and_cut[:,4],sorted_and_cut[:,5],sorted_and_cut[:,6], sorted_and_cut[:,7])  #
    nse_b_ts[i,1]   = 1 .- nse_b_ts[i,1]
end



sorted_and_cut = cut_and_sort(error, nse, Qbs_sum, data_sum, times,index_of_times, Dm_50, fsl, smoothing, hooke, base_cor)
tot_error_b_ts   =  return_best(sorted_and_cut[:,1],sorted_and_cut[:,end-3],sorted_and_cut[:,end-2],sorted_and_cut[:,end-1], sorted_and_cut[:,end])  #
sort_stdy_pd_err = sortslices(sorted_and_cut,
                              dims=1,
                               by=x->(x[1]))
sort_abs_err = sortslices(sorted_and_cut,
                          dims=1,
                          by=x->(x[3]))



println("
        $(glacier)
        number of runs: $(length(grid_search[:,1]))
        number of runs that made cut: $(length(sorted_and_cut[:,1]))\n

        Best NSE of all runs: $(maximum(nse_b_ts[:,1]))\n

        NSE\n
        best nse: $(nse_b_ts[index_of_times,1])
        dm_50: $(nse_b_ts[index_of_times,2])
        fsl: $(nse_b_ts[index_of_times,3])
        smoothing: $(nse_b_ts[index_of_times,4])
        hooke: $(nse_b_ts[index_of_times,5]) \n

        STUDY PERIOD
        smallest study period error: $(sort_stdy_pd_err[1,1])
        dm_50: $(sort_stdy_pd_err[1,4])
        fsl: $(sort_stdy_pd_err[1,5])
        smoothing: $(sort_stdy_pd_err[1,6])
        hooke: $(sort_stdy_pd_err[1,7]) \n

        ABSOLUTE ERROR
        smallest abs err: $(sort_abs_err[1,3])
        dm_50: $(sort_abs_err[1,4])
        fsl: $(sort_abs_err[1,5])
        smoothing: $(sort_abs_err[1,6])
        hooke: $(sort_abs_err[1,7])
                ")

if plotting== false
    error()
end


if glacier == "gorner"
    abs_err_limit = [minimum(abs.(sorted_and_cut[:,3]))-0.1*minimum(abs.(sorted_and_cut[:,3])), maximum(abs.(sorted_and_cut[:,3]))+0.1*maximum(abs.(sorted_and_cut[:,3]))]
    err_limit = [ minimum(abs.(sorted_and_cut[:,1]))-0.1* minimum(abs.(sorted_and_cut[:,1])), maximum(abs.(sorted_and_cut[:,1]))+0.1*maximum(abs.(sorted_and_cut[:,1]))]
else
    abs_err_limit = [minimum(abs.(sorted_and_cut[:,3]))-0.1*minimum(abs.(sorted_and_cut[:,3])), maximum(abs.(sorted_and_cut[:,3]))+0.1*maximum(abs.(sorted_and_cut[:,3]))]
    err_limit = [ minimum(abs.(sorted_and_cut[:,1]))-0.1* minimum(abs.(sorted_and_cut[:,1])), maximum(abs.(sorted_and_cut[:,1]))+0.1*maximum(abs.(sorted_and_cut[:,1]))]

end


scale = 1000


##############
### t-step ###
##############

# figure("error_nse_tstep", figsize=(5.5,5.5))
# ax1=subplot(1,1,1)
# p = plot(times'./3600, error_b_ts[:,1]./scale.*times',"bo")#s=ms,c="red", edgecolor="None",zorder=3)
# ylabel(L"ERR (1000 m$^{3}$)", fontsize=fs, color="b")
# tick_params(axis="both" ,labelsize=fs)
# if glacier == "gorner"
#     PyPlot.ylim((abs_err_limit./scale*0.85))
# else
#         PyPlot.ylim((abs_err_limit./scale*0.6))
# end   #PyPlot.xlim((err_limit./scale))
# xlabel("time period (hrs)",fontsize=fs)

# ax2 = ax1[:twinx]()
# ax2[:spines]["right"][:set_position]
# # for i=1:length(times)
# #     axvline(times[i]'./3600,0, nse_b_ts[i,1]-0.025,linewidth=0.5,color="grey")
# # end

# plot(times'./3600, nse_b_ts[:,1],"rs")#s=ms,c="black", edgecolor="None",zorder=3)
# ylabel("NSE",fontsize=fs, color="r")
# tick_params(axis="both" ,labelsize=fs)
# PyPlot.ylim(0, 1.05)
# title("$(glacier)gletscher",fontsize=fs*1.25)
# tight_layout()

# savefig("$(fp_save)/$(glacier)_error_nse_tstep.pdf") # saving way too many files... not neat.


#############################
### t-step with empirical ###
#############################
figure("error_nse_tstep_emp", figsize=(8,4))
ax1=subplot(1,2,1)

plot(times'./84600, nse_b_ts[:,1],"bo")#s=ms,c="black", edgecolor="None",zorder=3)
plot(times'./84600, NSE_emp,"rs")#s=ms,c="black", edgecolor="None",zorder=3)
ylabel(L"NSE_{\mathrm{k}}",fontsize=fs)
xlabel("time aggregation (days)",fontsize=fs)
tick_params(axis="both" ,labelsize=fs)
PyPlot.ylim(0, 1.05)



subplot(1,2,2)
plot(times'./84600, error_b_ts[:,1]./scale,"bo", label="physical model")#s=ms,c="red", edgecolor="None",zorder=3)
plot(times'./84600, ERR_emp./scale,"rs", label="empirical model")#s=ms,c="red", edgecolor="None",zorder=3)
ylabel(L"$ERR_{\mathrm{k}}$ (1000 m$^{3}$)", fontsize=fs)
tick_params(axis="both" ,labelsize=fs)
if glacier == "gorner"
    legend(fontsize=fs,loc=1)
end

if glacier == "gorner"
    PyPlot.ylim((0,57))
elseif glacier == "aletsch"
    PyPlot.ylim((0,220))
end
tight_layout()
xlabel("time aggregation (days)",fontsize=fs)


if save== true
    savefig("$(fp_save)/$(glacier)_error_nse_tstep_emp.pdf") # saving way too many files... not neat.
end
#############
### error ###
#############

figure("error", figsize=(5.5,5.5))
ax1=subplot(1,1,1)
p = plot(abs.(sorted_and_cut[:,1])./scale, abs.(sorted_and_cut[:,3])./scale,"bo")#s=ms,c="red", edgecolor="None",zorder=3)
ylabel(L"$\mathrm{ERR_{24h}}$ (1000 m$^{3}$)", fontsize=fs,color="b")
tick_params(axis="both" ,labelsize=fs)
PyPlot.ylim((abs_err_limit./scale))#abs_err_limit)
PyPlot.xlim((err_limit./scale))
xlabel(L"error (1000 m$^{3}$)",fontsize=fs)

ax2 = ax1[:twinx]()
ax2[:spines]["right"][:set_position]
plot(abs.(sorted_and_cut[:,1])./scale, abs.(sorted_and_cut[:,2]),"rs")#s=ms,c="black", edgecolor="None",zorder=3)
ylabel(L"$\mathrm{NSE_{24h}}$", fontsize=fs, color="r")
tick_params(axis="both" ,labelsize=fs)
PyPlot.ylim(0, 1.05)
title("$(glacier)gletscher-$(Int(times[index_of_times]/3600)) h resolution",fontsize=fs*1.25)
tight_layout()

if save== true
    savefig("$(fp_save)/$(glacier)_hires_error.pdf") # saving way too many files... not neat.
end

##############################
### grid search processing ###
##############################

figure("parameter-grid", figsize=(10,4))
subplot(1,3,1)
scatter(Dm_50,smoothing,s=ms, c="red", edgecolor="None",zorder=3)
scatter(sorted_and_cut[:,4],sorted_and_cut[:,7],s=ms,c="blue", edgecolor="None",zorder=3)
ylabel("smoothing period (hrs)",fontsize=fs)
tick_params(axis="both" ,labelsize=fs)
xlabel("Dm_50 (m)",fontsize=fs)
tight_layout()

subplot(1,3,2)
scatter(smoothing,fsl,s=ms, c="red", edgecolor="None",zorder=3)
scatter(sorted_and_cut[:,6],sorted_and_cut[:,5],s=ms,c="blue", edgecolor="None",zorder=3)
xlabel("smoothing period  (hrs)",fontsize=fs)
tick_params(axis="both" ,labelsize=fs)
ylabel("sliding fraction",fontsize=fs)
title("$(glacier)gletscher parameters", fontsize=fs*1.25)
tight_layout()

# subplot(1,4,3)
# scatter(fsl,Dm_50,s=ms, c="red", edgecolor="None",zorder=3)
# scatter(sorted_and_cut[:,5],sorted_and_cut[:,4],s=ms,c="blue", edgecolor="None",zorder=3)
# tick_params(axis="both" ,labelsize=fs)
# xlabel("sliding fraction",fontsize=fs)
# ylabel("grain size (m)",fontsize=fs)
# tight_layout()

subplot(1,3,3)
scatter(smoothing,Dm_50,s=ms, c="red", edgecolor="None",zorder=3)
scatter(sorted_and_cut[:,6],sorted_and_cut[:,4],s=ms,c="blue", edgecolor="None",zorder=3)
tick_params(axis="both" ,labelsize=fs)
xlabel("smoothing period (hrs)",fontsize=fs)
ylabel(L"Hooke angle ($\circ$)",fontsize=fs)
tight_layout()



## parameter dependency ##

figure("parameter-abs_err", figsize=(5,7))

ax=subplot(3,1,1)
scatter(sorted_and_cut[:,3]./scale,sorted_and_cut[:,6],s=ms,c="black", edgecolor="None",zorder=3)
ylabel(L"$\Delta t_{Qw}$ (h)",fontsize=fs)
#PyPlot.ylim(0.0, abs_err_limit)
PyPlot.xlim((abs_err_limit./scale))
fill_between(abs_err_limit./scale ,minimum(smoothing),maximum(smoothing), color="lightgrey")
#title("$(glacier)gletscher",fontsize=fs*1.25)
ylim((minimum(smoothing)-3, maximum(smoothing)+10))
tick_params(axis="y" ,labelsize=fs)
setp(ax[:get_xticklabels](),visible=false) # Disable x tick labels
subplots_adjust(hspace=0.0)
tight_layout()


ax2=subplot(3,1,2, sharex=ax)
scatter(sorted_and_cut[:,3]./scale, sorted_and_cut[:,5],s=ms, c="black", edgecolor="None",zorder=3)
ylabel(L"$f_{sl}$",fontsize=fs)
fill_between(abs_err_limit./scale ,minimum(fsl),maximum(fsl),color="lightgrey")
PyPlot.ylim(minimum(fsl)-0.25,maximum(fsl)+.25)
PyPlot.xlim((abs_err_limit./scale))
tick_params(axis="y" ,labelsize=fs)
setp(ax2[:get_xticklabels](),visible=false) # Disable x tick labels
subplots_adjust(hspace=0.0)


subplot(3,1,3, sharex=ax)
scat=scatter(sorted_and_cut[:,3]./scale,sorted_and_cut[:,4],s=ms,c="black", edgecolor="None",zorder=3)
ylabel(L"Dm$_{50}$ (m)",fontsize=fs)
fill_between(abs_err_limit./scale ,minimum(Dm_50),maximum(Dm_50),color="lightgrey")
ylim(0,maximum(Dm_50)+0.05)
tick_params(axis="both" ,labelsize=fs)
PyPlot.xlim((abs_err_limit./scale))
xlabel(L"$ERR_{24\mathrm{h}}$ (1000 m$^{3}$)",fontsize=fs)
tight_layout()
# cb = colorbar(scat, orientation = "horizontal", ticks=[base_cor, (1-0.5*base_cor), 1])
# cb[:ax][:set_xticklabels](["NSE: $(base_cor)",
#                            "NSE: $((1-base_cor*.5))",
#                            "NSE: 1"],
#                            fontsize=15)

# subplots_adjust(hspace=0.0)
if save== true
    savefig("$(fp_save)/$(glacier)_hi-res_error_parameters.pdf") # saving way too many files... not neat.
end
