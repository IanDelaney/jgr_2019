# """""
# for gries data (germ output), in CSV form
# """""
# function runoffsort_gries!(runoff)
#     Q_meas = runoff[:,4]
#     runoff_date=zeros(length(Q_meas))

#     for i = 1:length(Q_meas)
#         runoff_date[i]=runoff[i,1] .* SUGSET.year+runoff[i,2].* SUGSET.day - 1970 .* SUGSET.year
#     end

#     return Q_meas, runoff_date
# end

"""
sort runoff_data for Gorner
"""
function runoffsort_gorner!(runoff)
    runoff_vol = runoff[:,2]
    Q_meas = zeros(length(runoff_vol))

    for i = 1:length(Q_meas)
        Q_meas[i] = parse(Float64,runoff_vol[i])
    end

    # x= VAWTools.boxcar(Q_meas,10)

    for i=1:length(Q_meas)
        if Q_meas[i] < 2
            Q_meas[i]= x[i]
        end
    end


    runoff_date_= runoff[:,1]# Array{String}(size(runoff,1))

 #    for i = 1:size(runoff,1)
#         runoff_date_[i]=runoff[i,1]*runoff[i,2]
#     end
# \
    runoff_date = datetime2unix.(DateTime.(runoff_date_,"yyyy-mm-dd HH:MM:SS")) ## datetime2unix converts to seconds


    return Q_meas, runoff_date
end

"""
sort runoff_data for Aletsch/Massa
"""
function runoffsort_aletsch!(runoff)
    #runoff_vol = runoff[:,2]
    Q_meas = zeros(length(runoff[:,2]))

    for i = 1:length(Q_meas)
        Q_meas[i]=parse(Float64,runoff[i,2])
    end


    runoff_date_= Array{String}(size(runoff,1))

    for i = 1:size(runoff,1)
        runoff_date_[i]=runoff[i,1]#*runoff[i,2]
    end
    runoff_date =   datetime2unix.(DateTime.(runoff_date_,"yyyy.mm.dd HH:MM"))

    return Q_meas, runoff_date
end


"""
sort runoff_data for fiescher
"""
function runoffsort_fiescher!(runoff)
    #runoff_vol = runoff[:,2]
    Q_meas = zeros(length(runoff[:,2]))

    for i = 1:length(Q_meas)
        Q_meas[i]=parse(Float64,runoff[i,2])
    end


    runoff_date_= Array{String}(size(runoff,1))

    for i = 1:size(runoff,1)
        runoff_date_[i]=runoff[i,1]
    end

    runoff_date =   datetime2unix.(DateTime(runoff_date_,"dd-mm-yyyy HH:MM:SS"))

    return Q_meas, runoff_date
end


"""
sort runoff_data for standardized format
"""
function runoff_b0_sort!(data)
    meas = zeros(length(data[:,2]))

    for i = 1:length(meas)
          meas[i]=parse(Float64,data[i,2])
    end


    date_= Array{Any}(undef,size(data,1))


    for i = 1:size(data,1)
        date_[i]=data[i,1]
    end

    date =   datetime2unix.(DateTime.(date_,"yyyy-mm-dd HH:MM:SS"))

    whole_thing = sortslices([date meas],dims=1,by=x->(x[1]))


    return whole_thing[:,2], whole_thing[:,1]
end
