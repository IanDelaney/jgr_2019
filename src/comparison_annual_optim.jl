############
# script to compare model runs to real data...
# an optimization scheme is added as to speedup calibration as a whole
using JLD
using DelimitedFiles
using Statistics
using Dierckx
using VAWTools
using Parameters
using Interpolations
using Dates
using StatsBase
using SmoothingSplines
using Dierckx
#using MathProgBase -> not sure I need this
import Roots: find_zero
using SUGSET
using SUGSET: erosion, post_proc, solvers
import SUGSET: zb, zs, gradzb, gradzs, source_water, width, source_till, thick, find_sp,b_0_determ, spinner, source_water_ddm,dht_dt_fn, Q_w, NSE, NSE_log, day, year

include("comparison_functions.jl")
#include("../save_run.jl")

hydro_type = ["steady","ddm","overdeepening", "future_evolution","real"][5] # only used for synthetic runs... usuall use ddm

experiment = "annual"
run_name = "annual"
glacier=["gries" "gorner"][2]

save = [true false][1]
evolve=[true false][2]
scheme = ["Grid" "Optim" "ParamFunc" "Compare"][end]

println("\n site: $(glacier)\n
        scheme: $(scheme)")
format_="$(glacier)_deltahneg4_Qtop20_0rs"  # title of model run

# fp="/scratch_net/esker/delaneyi/sed_modeling_data/$(glacier)/" # file path to data on scratch disk

if homedir()=="/home/mauro"
    fp="/home/mauro/netfs/vaw/root"*fp
elseif homedir()== "/Users/ian"
    fp="/Users/ian/data/sed_modeling_data/$(glacier)/" # file path to data on Ian's computer
    fp_save = "/Users/ian/eth/subglacial_sed/modeling/results/"

elseif homedir()== "/Users/idelaney"
    fp="/Users/idelaney/data/sed_modeling_data/$(glacier)/" # file path to data on Ian's computer
    fp_save = "/Users/idelaney/research/subglacial_sed/modeling/results/"

elseif homedir()== "/home/idelaney"
    fp="/home/idelaney/data/sed_modeling_data/$(glacier)/" # file path to data on Ian's computer
    fp_save = "/home/idelaney/sed_transport/model_outputs/"
elseif homedir()=="/home/delaneyi"
    fp="/scratch_net/esker/delaneyi/sed_modeling_data/$(glacier)/" # file path to data on scratch disk
    fp_save = "/home/delaneyi/analysis/sed_modeling_outputs/"
elseif homedir()=="/home/werderm"
    fp="/scratch_net/esker/delaneyi/sed_modeling_data/$(glacier)/" # file path to data on scratch disk
    fp_save = "/home/werderm/tmp/sugset"
end


#################
### Variables ###
#################

# good Gorner combo.
if glacier == "gorner"

    # varies =[ 0.062 #Dm... grain size
    #           .15 #sliding fraction... MAKE SURE THIS IS BETWEEN 0 AND 1
    #           53.0 #smoothing factor
    #           ]
     varies =[0.062 #Dm... grain size
              1.725 #sliding fraction... MAKE SURE THIS IS BETWEEN 0 AND 1
              102.0 #smoothing factor
              ]

    #good Gries combo.
elseif glacier == "gries"
    varies =[0.15125 #Dm... grain size
             0.36 #sliding fraction... MAKE SURE THIS IS BETWEEN 0 AND 1
             53.0 #smoothing factor (hrs)
             ]
end


lower = [0.0025#Dm... grain size
         0.15 #sliding fraction...
         5 	      #smoothing factor
	 0         #access_fac
         ]

upper = [0.3 #Dm... grain size
         2.25 #sliding fraction...
         24*7 #smoothing factor (hrs)
	 1 #access_fac
         ]

# variables for glacier
pp = Phys(Dm = varies[1])


include("real_glacier_processing.jl")

pg = RealGlacier(name=glacier,
                 fsl = varies[2],
                 Q_sm = Int(varies[3]),
                 tspan=(runoff_date_mod[1],runoff_date_mod[end]),
                 tspan_spin = (runoff_date_spin[1],runoff_date_spin[end]),
                 tspan_run= (runoff_date_mod[1],runoff_date_mod[end]),
                 spl_surf = spl_surf,
                 spl_bed = spl_bed,
                 int_width = int_width
                 )
pn = Num()


##############################
### Take care of hydrology ###
##############################

pg = RealGlacier(pg,
                 spin=true,
                 int_b_0_spin =  Spline1D(runoff_date_spin, pg.b_0_spin; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11),
                 int_b_0_sm_spin = Spline1D(runoff_date_spin,
                                            Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                              pg.b_0_spin,
                                                              (Int(div(pg.Q_sm,2)*2+1),)
                                                              )
                                            ; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11
                                            ))

pg = RealGlacier(pg,
                 tspan=pg.tspan_spin,
                 int_b_0    = Spline1D(runoff_date_mod,pg.b_0; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11),
                 int_b_0_sm =  Spline1D(runoff_date_mod,
                                        Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                              pg.b_0,
                                                              (Int(div(pg.Q_sm,2)*2+1),) # make odd
                                                              )
                                        ; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11
                                        )
                 )


#############
### Data  ###
#############

if glacier == "gries"
    # @assert glacier == 'gries'
    include("gries_annual.jl")
elseif glacier == "gorner"
    include("gorner_annual.jl")
end

############
### Grid ###
############

#if Optimizer == "Loop" # loop over large parameter space...


if scheme == "Grid"
    include("grid_search_pmap.jl")
    walltimes, vari_1, vari_2, vari_3,vari_4, model_abs_err, model_err_sum, model_rank_cor, runs =
        grid_search_annual(pp, pg, pn, data, runoff_date_spin, runoff_date_mod, fp_save)

elseif scheme == "Optim"
    using Optim
    od = OnceDifferentiable(v -> solve_funct(v,data),varies)

    res = Optim.optimize(od,
                         varies,
                         lower,
                         upper,
                         NelderMead(),
                         #Fminbox{GradientDescent}()
                         )

    new_varies=Optim.minimizer(res)

     summed_model_out = data_sums(data,Qbs,pg)

elseif scheme == "ParamFunc"


elseif scheme == "Compare"
    pg = RealGlacier(pg,
                     spin = true, # set spin == to true to call proper water functions
                     tspan=pg.tspan_spin
                     )
    pg = RealGlacier(pg,
                     tout=pg.tspan[1]:.2*day:pg.tspan[2] # output times
                     )


    # spin up
    println("beginning spin")
    hts_evol,spins = spinner(RealGlacier,pg,pp,pn,0.75e-3)


    # pass in hts to pg
    pg = RealGlacier(pg,
                     spin = false,
                     ht0=hts_evol,
                     tspan=pg.tspan_run) # initial till layer thickness

    pg = RealGlacier(pg,
                     tout=pg.tspan[1]:.2*day:pg.tspan[2] # output times
                     )

    sol  =  run_model(pg, pp, pn) # run model

    # post-proc between here and...

    hts, dht_dts, Qws, Qws_sm, Qbs, Qbes = post_proc(sol,pg, pp, pn)

    # ... here
    hts = sol(pg.tout)

    summed_model_out = data_sums(data,Qbs[1,:],pg)

    err_sum =sum(data[:,3])-sum(summed_model_out)

    abs_err = SUGSET.abs_error(data[:,3],summed_model_out)

    rank_cor = corspearman(summed_model_out,data[:,3])

    println(glacier,scheme)
    println("\n Error: $(err_sum)  Dm50:$(varies[1])   fsl:$(varies[2])  Q_sm:$(varies[3]) \n")


    if save == true


        smid=zeros(length(pg.sgrid)-1)

        for i =length(pg.sgrid)-1:-1:1
            smid[i] = (pg.sgrid[i+1] + pg.sgrid[i])/2 #non stagard grid
        end

        w = zeros(length(smid))

        for i =1:length(smid)
            w[i]= width(smid[i],0,pg)
        end


        source_year =   SUGSET.erosion.(smid, Ref(pg.tout[1]), Ref(pg), Ref(pp))*year

        zs_= zs.(pg.sgrid,Ref(0),Ref(pg))
        zb_= zb.(pg.sgrid,Ref(0),Ref(pg))

        ###########################
        ### save test variables ###
        ###########################

        #    save("$(fp_save)$(run_name)_dm$(pp.Dm)_ht0$(round(mean(pg.ht0),2)).jld",
        JLD.save("$(fp_save)$(glacier)_annual_$(varies[1])_$(varies[2])_$(varies[3]).jld",
                 "zs",    zs_, #geometry
                 "zb",    zb_,
                 "width", w,
                 "sed_flux_meas", data[:,3],
                 "summed_model_out", summed_model_out,
                 "source_year", source_year,
                 "tout",       pg.tout, #parameters
                 "sgrid",      pg.sgrid,
                 "ht0",        pg.ht0,
                 "fsl",        pg.fsl,
                 "Dm",         pp.Dm,
                 "lr",         pp.lr,
                 "fi",         pp.fi,
                 "Qws",   Qws, #outputs
                 #"sedflux_rs", sedflux_rs,
                 #"dphi_dx", dphi_dx,
                 "Qws_sm",Qws_sm,
                 "Qbes",  Qbes,
                 "hts",   hts,
                 "Qbs",   Qbs,
                 )
    end

else
    error("Come on!!!!")
end
