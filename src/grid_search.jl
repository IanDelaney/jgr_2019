# a script wiht a bunch of nested for loops to grid search for different parameters.
# right now 4 parameters are included

n_1 = 25 #Dm_50
n_2 = 20 #fsl
n_3 = 25 #smoothin'

n_1 = 5 #Dm_50
n_2 = 5 #fsl
n_3 = 5 #smoothin'

v_1 = linspace(lower[1], upper[1],n_1) # Dm_50
v_2 = linspace(lower[2], upper[2],n_2)   # sliding fraction
v_3 = div.((linspace(lower[3], upper[3],n_3))*2,2)  # smoothing

# annual
model_abs_err =[]
model_err_sum = []
model_rank_cor =[]

# hi-res
model_Qbs_sum = []
data_writer = []
out_err_02day = []
out_err_025day =[]
out_err_03day= []
out_err_04day= []
out_err_05day= []
out_err_075day= []
out_err_day= []
out_err_125day= []
out_err_2day= []
out_err_250day= []
out_err_3day= []
out_err_5day= []
out_err_7day= []
out_nse_02day= []
out_nse_025day= []
out_nse_03day= []
out_nse_04day= []
out_nse_05day= []
out_nse_075day= []
out_nse_day= []
out_nse_125day= []
out_nse_2day= []
out_nse_250day= []
out_nse_3day= []
out_nse_5day= []
out_nse_7day=[]


v_1_ =[] #holder for file writing
v_2_ =[] #holder for file writing
v_3_ =[] #holder for file writing

count = 0

for i = 1:n_1 # Dm_50
    for j = 1:n_2 #Fi
        for k = 1:n_3 #sliding fraction
            tic()
            println("\n$(glacier)-$(scheme)")

            #global count
            count::Int += 1
            println("run: $(count)   total runs: $(n_1*n_2*n_3)")

            ## redefine variables
            pp = Phys(pp,
                      Dm = v_1[i],
                      )

            pg = RealGlacier(pg,
                             fsl = v_2[j],
                             Q_sm=Int(round(v_3[k])),
                             b_0_sm_spin = boxcar(pg.b_0_spin,(Int(round(v_3[k])),0), ones(length(pg.b_0_spin))),

                             int_b_0_spin = interpolate((runoff_date_spin,),boxcar(pg.b_0_spin,2)        , Interpolations.Gridded(Constant())),
                             int_b_0_sm_spin = interpolate((runoff_date_spin,), boxcar(pg.b_0_spin,(Int(round(v_3[k])),0), ones(length(pg.b_0_spin)))
                                                           , Interpolations.Gridded(Constant())),

                             int_b_0 = interpolate((runoff_date_mod,),boxcar(pg.b_0,2), Interpolations.Gridded(Constant())),
                             int_b_0_sm = interpolate((runoff_date_mod,),
                                                      boxcar(pg.b_0,
                                                             (Int(round(v_3[k])),0),
                                                             ones(length(pg.b_0))
                                                             )
                                                      , Interpolations.Gridded(Constant()))
                             )

            println("\n Dm50:$(pp.Dm)  fsl:$(pg.fsl)  Q_sm:$(pg.Q_sm) \n")

            ## re-run the model with new variables
            if experiment == "annual"

                #try
                    err_sum, rank_cor, abs_err = comparerer_annual(data,pg,pp,pn)

                    push!(model_err_sum, err_sum)
                    push!(model_rank_cor, rank_cor)
                    push!(model_abs_err, abs_err)
                # catch
                #     err_sum = Inf
                #     rank_cor =Inf
                #     abs_err = Inf

                #     push!(model_err_sum, err_sum)
                #     push!(model_abs_err, abs_err)
                #     push!(model_rank_cor, rank_cor)
                #  end

                ## save variable and result to file
                push!(v_1_, v_1[i])
                push!(v_2_, v_2[j])
                push!(v_3_, v_3[k])

                println("\n error: $(model_err_sum[end])")
                println("\n spearman: $(model_rank_cor[end])")

                headers= [           "'error (sum)'" "'rank_cor(spearman)'" "'abs_err'"      "'Dm_50'"   "'fsl'"  "'smoothing'"]
                file = vcat(headers,[model_err_sum       model_rank_cor         model_abs_err          v_1_       v_2_         v_3_ ])

            else

                try

                    err_02day, err_025day,  err_03day, err_04day, err_05day,  err_075day, err_day, err_125day, err_2day, err_250day, err_3day, err_5day, err_7day, nse_02day, nse_025day,  nse_03day, nse_04day, nse_05day,  nse_075day, nse_day, nse_125day, nse_2day, nse_250day, nse_3day, nse_5day, nse_7day, Qbs_sum  = comparerer(sedflux_rs,global_date,pg,pp,pn)

                    push!(model_Qbs_sum , Qbs_sum)
                    push!(data_writer , sum(abs.(sedflux_rs[1:end-1])))
                    push!(out_err_02day , err_02day)
                    push!(out_err_025day, err_025day)
                    push!(out_err_03day,  err_03day)
                    push!(out_err_04day,  err_04day)
                    push!(out_err_05day,  err_05day)
                    push!(out_err_075day, err_075day)
                    push!(out_err_day,    err_day)
                    push!(out_err_125day, err_125day)
                    push!(out_err_2day,   err_2day)
                    push!(out_err_250day, err_250day)
                    push!(out_err_3day,   err_3day)
                    push!(out_err_5day,   err_5day)
                    push!(out_err_7day,   err_7day)
                    push!(out_nse_02day,  nse_02day)
                    push!(out_nse_025day, nse_025day)
                    push!(out_nse_03day,  nse_03day)
                    push!(out_nse_04day,  nse_04day)
                    push!(out_nse_05day,  nse_05day)
                    push!(out_nse_075day, nse_075day)
                    push!(out_nse_day,    nse_day)
                    push!(out_nse_125day, nse_125day)
                    push!(out_nse_2day,   nse_2day)
                    push!(out_nse_250day, nse_250day)
                    push!(out_nse_3day,   nse_3day)
                    push!(out_nse_5day,   nse_5day)
                    push!(out_nse_7day,   nse_7day)


                catch
                    push!(model_Qbs_sum , Inf)
                    push!(data_writer , Inf)
                    push!(out_err_02day , Inf)
                    push!(out_err_025day, Inf)
                    push!(out_err_03day,  Inf)
                    push!(out_err_04day,  Inf)
                    push!(out_err_05day,  Inf)
                    push!(out_err_075day, Inf)
                    push!(out_err_day,    Inf)
                    push!(out_err_125day, Inf)
                    push!(out_err_2day,   Inf)
                    push!(out_err_250day, Inf)
                    push!(out_err_3day,   Inf)
                    push!(out_err_5day,   Inf)
                    push!(out_err_7day,   Inf)
                    push!(out_nse_02day,  Inf)
                    push!(out_nse_025day, Inf)
                    push!(out_nse_03day,  Inf)
                    push!(out_nse_04day,  Inf)
                    push!(out_nse_05day,  Inf)
                    push!(out_nse_075day, Inf)
                    push!(out_nse_day,    Inf)
                    push!(out_nse_125day, Inf)
                    push!(out_nse_2day,   Inf)
                    push!(out_nse_250day, Inf)
                    push!(out_nse_3day,   Inf)
                    push!(out_nse_5day,   Inf)
                    push!(out_nse_7day,   Inf)
                end

                ## save variable and result to file
                push!(v_1_, v_1[i])
                push!(v_2_, v_2[j])
                push!(v_3_, v_3[k])

                headers= ["err_02day" "err_025day"  "err_03day" "err_04day" "err_05day"  "err_075day" "err_day" "err_125day" "err_2day" "err_250day" "err_3day" "err_5day" "err_7day" "nse_02day" "nse_025day"  "nse_03day" "nse_04day" "nse_05day"  "nse_075day" "nse_day" "nse_125day" "nse_2day" "nse_250day" "nse_3day" "nse_5day" "nse_7day" "Qbs_sum" "data_sum" "Dm_50"  "fsl"  "smoothing"]

                a= [out_err_02day out_err_025day out_err_03day out_err_04day out_err_05day out_err_075day out_err_day out_err_125day out_err_2day out_err_250day out_err_3day out_err_5day out_err_7day out_nse_02day out_nse_025day out_nse_03day out_nse_04day out_nse_05day out_nse_075day out_nse_day out_nse_125day out_nse_2day out_nse_250day out_nse_3day out_nse_5day out_nse_7day model_Qbs_sum data_writer  v_1_ v_2_  v_3_]

                file = vcat(headers,a)


            end

            writedlm("$(fp_save)/$(glacier)_$(experiment)_results_may31_b.txt", file,"\t")
            #writedlm("/Users/ian/Desktop/$(glacier)_analysis_d_results.txt", file,"\t")
            toc()

        end
    end
end
