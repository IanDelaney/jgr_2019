# a script to make test results.
# run with runtest.jl outputs in REP0
# test

using JLD

###########################
### make test variables ###
###########################

### Geometry ###
function save_run(sol, pg, pp, pn, run_name,fp_save;verbose=false)
    @unpack sgrid, ht0, tspan, tout = pg

    zs_=[]
    zb_=[]
    for j = 1:length(pg.sgrid)
        push!(zs_, zs(pg.sgrid[j],pg.tout[1],pg))
        push!(zb_, zb(pg.sgrid[j],pg.tout[1],pg))
    end

    #verbose && (println("post-proc start"); tic())
    # post-proc
    hts     =  sol(tout)
    Qws     =  zeros(length(sgrid), length(tout))
    Qws_sm  =  zeros(length(sgrid), length(tout))
    Qbs     =  zeros(length(sgrid), length(tout))
  #  dphi_dx =  zeros(length(sgrid), length(tout))
    Qbes    =  zeros(length(sgrid), length(tout))
    dht_dts =  zeros(length(sgrid)-1, length(tout))

    for i in eachindex(tout)
        t = tout[i]
        Qws[:,i] = SUGSET.Q_w(sgrid, t, pg, pp)
        Qws_sm[:,i] = SUGSET.Q_w_sm(sgrid, t, pg, pp)
        dht_dts[:,i],Qbs[:,i],Qbes[:,i] = SUGSET.dht_dt_fn(hts[i], sgrid, t, pg, pp, pn)
    end


    dt = (pg.tout[end]-pg.tout[1])/(length(pg.tout))
    smid, ds = SUGSET.get_smid_ds(pg.sgrid)
    V_source_rate=zeros(length(pg.tout))

for (i,t) in enumerate(pg.tout)
    ht = sol(t)
    V_source_rate[i] = sum(SUGSET.effective_sed_source.(smid, Ref(t), Ref(pg), Ref(pp), Ref(pn), ht).*SUGSET.width.(smid,t,Ref(pg)).*ds)
end


    hts = hcat(hts...)
    #verbose && (println("post-proc end"); toc())

    ###########################
    ### save test variables ###
    ###########################

        save("$(fp_save)$(run_name)_dm$(pp.Dm)_ht0$(round(mean(pg.ht0), digits=2)).jld",
             #save("$(fp_save)$(glacier)_$(run_name).jld",
         "zs",    zs_, #geometry
         "zb",    zb_,

         "tout",       pg.tout, #parameters
         "sgrid",      pg.sgrid,
         "ht0",        pg.ht0,
         "fsl",        pg.fsl,
         "Dm",         pp.Dm,
         "lr",         pp.lr,
         "fi",         pp.fi,
         "DT",         pp.DT,
         "DAMP",       pp.DAMP,
         "DDAMP",      pp.DDAMP,

         "Qws",   Qws, #outputs
         #"dphi_dx", dphi_dx,
         "Qws_sm",Qws_sm,
         "Qbes",  Qbes,
         "hts",   hts,
         "Qbs",   Qbs,
         )
end
