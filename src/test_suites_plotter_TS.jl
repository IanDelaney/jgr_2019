using StatsBase
using Statistics
using JLD
using PyPlot
using VAWTools
using Images
using SUGSET
import SUGSET: day, year

fp ="/Users/idelaney/research/subglacial_sed/modeling/test_cases/"
fp_save ="/Users/ian/eth/subglacial_sed/paper/figs/"
fp_save ="/Users/idelaney/research/subglacial_sed/modeling/test_cases/"

# const SUGSET.day = 24*3600. # secs in day
# const year = day*365 # secs in year

comment = ["_dm0.01_ht00.0"
           "_dm0.0075_ht00.05"
           "_dm0.04_ht00.0"
           ][3]

suites =  [#"temp_offset_bench"
           # "B_0_term_test_bench"
           "vari_bench"#
           ]


runs= [5  5  4 4]
fs = 18

colors_to_use =["blue"
                "red"
                "orange"
                "green"
                "purple"]

time_period =["long" "short"][2]


for i = 1:length(suites)
    for j = 1:runs[i]


        if suites[i]=="temp_offset_bench" # do this last

            run_output=load("$(fp)$(suites[i])_$(j)$(comment).jld")

            zs     = run_output["zs"]
            zb     = run_output["zb"]

            tout   = run_output["tout"]
            sgrid  = run_output["sgrid"]
            DT     = run_output["DT"]
            DAMP   = run_output["DAMP"]
            DDAMP  = run_output["DDAMP"]

            Qws =    run_output["Qws"][1,:]
            Qws_sm = run_output["Qws_sm"][1,:]
            Qbes  =  run_output["Qbes"][1,:]
            hts =    mean(run_output["hts"][1:end,:],dims=1)'
            Qbs =    run_output["Qbs"][1,:]


            if time_period == "short"
               ts = length(tout)- length(tout)÷15+400:length(tout)-400

            else
                ts = 1: (length(tout)÷15)*6 # take first 6 years
            end

            # find sumation
            t_diff = tout[2]-tout[1]
            Qbs_sum = abs(sum(Qbs[ts])*t_diff)
            Qws_sum = abs(sum(Qws[ts])*t_diff)
            println("temp: $(DT)
                             Qbs: $(Qbs_sum)
                             Qws: $(Qws_sum)
                             conc: $(Qbs_sum*1500 ./Qws_sum)
                             timedif:$(t_diff./86400)"

                    )

            figure("$(suites[i])_$(comment)", figsize=(8*1.6,6))
            ax=subplot(3,1,1)

            if time_period == "short"
                plot(tout[ts]/year, -Qws[ts], linewidth="2", label ="Δ T: $(DT)", c = colors_to_use[j])# \n Qw sum:$(Qws_sum)")
            else
                plot(tout[ts]/year, Images.mapwindow(median!,-Qws[ts],(15,)), linewidth="2", label ="Δ T: $(DT)", c = colors_to_use[j])
                legend(loc=1,fontsize=fs*.75)
            end
            ylabel(L"$\mathrm{Q_w^0}$ $(\mathrm{m^3}$ $\mathrm{s^{-1}})$" ,fontsize=fs)
            tick_params(axis="both" ,labelsize=fs*.9)
            #xlim([tout[1], tout[end]])
            # ax[:yaxis][:set_ticks_position]("right")
            # ax[:yaxis][:set_label_position]("right")


            subplot(3,1,2, sharex=ax)
            if time_period == "short"
                plot(tout[ts]/year, -Qbs[ts], linewidth="2", c = colors_to_use[j] )#, label = "Qs sum: $(-Qbs_sum)")
            else
                plot(tout[ts]/year, Images.mapwindow(median!,-Qbs[ts],(15,)), linewidth="2", c = colors_to_use[j])#, label = "Qs sum: $(-Qbs_sum)")
            end
            ylabel(L"$\mathrm{Q_s^0}$ $(\mathrm{m^3}$ $\mathrm{s^{-1}})$" ,fontsize=fs)
            tick_params(axis="both" ,labelsize=fs*.9)


            # subplot(4,1,3,sharex=ax)
            # plot(tout[ts]/year,  hts[ts], linewidth="2", c = colors_to_use[j])
            # #ylim([0,0.3])
            # ylabel(L"$\mathrm{\overline{H_t}}$ ($\mathrm{m}$)",fontsize=fs)
            # xlabel("t (years)" ,fontsize=fs)
            # tick_params(axis="both" ,labelsize=fs*.9)
            # # ax[:yaxis][:set_ticks_position]("right")
            # # ax[:yaxis][:set_label_position]("right")

            subplot(3,1,3,sharex=ax)

            if time_period == "short"
                plot(tout[ts]/year, Images.mapwindow(extrema,-Qbes[ts]*1500 ./-Qws[ts],(21,)), linewidth="2", c = colors_to_use[j] )#, label = "Qs sum: $(-Qbs_sum)")
                #plot(tout[ts]/year, -Qbes[ts], linewidth="2", c = colors_to_use[j] )#, label = "Qs sum: $(-Qbs_sum)")
                #ylim([0,0.3])
                ylabel(L"$\mathrm{C_{Q_{sc}}^0}$ ($\mathrm{kg}\,\mathrm{m^{-3}}$)",fontsize=fs)
                xlabel("t (years)" ,fontsize=fs)
                tick_params(axis="both" ,labelsize=fs*.9)
                # ax[:yaxis][:set_ticks_position]("right")
                # ax[:yaxis][:set_label_position]("right")
                subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.99,
                                wspace=0.1, hspace=0.04)
            else
                plot(tout[ts]/year,  hts[ts], linewidth="2", c = colors_to_use[j])
                #ylim([0,0.3])
                ylabel(L"$\mathrm{\overline{H_t}}$ ($\mathrm{m}$)",fontsize=fs)
                xlabel("t (years)" ,fontsize=fs)
                tick_params(axis="both" ,labelsize=fs*.9)
                subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.99,
                                wspace=0.1, hspace=0.04)
                # ax[:yaxis][:set_ticks_position]("right")
                # ax[:yaxis][:set_label_position]("right")

            end

            if time_period == "short"
                savefig("$(fp_save)/$(suites[i])_$(comment).pdf") # saving way too many files... not neat.
            else
                savefig("$(fp_save)/$(suites[i])_$(comment)_long.pdf") # saving way too many files... not neat.
            end


        elseif suites[i]=="vari_bench" # do this last

            run_output=load("$(fp)$(suites[i])_$(j)$(comment).jld")

            zs     = run_output["zs"]
            zb     = run_output["zb"]

            tout   = run_output["tout"]
            sgrid  = run_output["sgrid"]
            DT     = run_output["DT"]
            DAMP   = run_output["DAMP"]
            DDAMP  = run_output["DDAMP"]

            Qws =    run_output["Qws"][1,:]
            Qws_sm = run_output["Qws_sm"][1,:]
            Qbes  =  run_output["Qbes"][1,:]
            hts =    mean(run_output["hts"][1:end,:],dims=1)'
            Qbs =    run_output["Qbs"][1,:]


if time_period == "short"
    ts = length(tout)- length(tout)÷15+450:length(tout)-450

else
    ts = 1: (length(tout)÷15)*6 # take first 6 years
end



            # find sumation
            t_diff = tout[2]-tout[1]
            Qbs_sum = Int(round(sum(Qbs[ts])*t_diff))
            Qws_sum = Int(round(sum(Qws[ts])*t_diff))
            println("vari: $(DDAMP)
                         Qbs: $(Qbs_sum)
                         Qws: $(Qws_sum)
                        conc: $(Qbs_sum*1500 ./ Qws_sum)"

            )

            Qws_max= zeros(length(ts))
            Qws_min= zeros(length(ts))

            for k =6:length(ts)-6
                Qws_max[k] = maximum(Qws[k-5:k+5])
                Qws_min[k] = minimum(Qws[k-5:k+5])
            end

            figure("$(suites[i])_$(comment)", figsize=(8*1.6,6))
            ax=subplot(3,1,1)
            if time_period == "short"
                plot(tout[ts]/year, -Qws[ts], linewidth="2",c = colors_to_use[j], label ="A d: $(DDAMP)")#  \n Qw sum:$(Qws_sum)")

            else
                plot(tout[ts]/year, -Qws[ts], linewidth="2",label ="A d: $(DDAMP)", c = colors_to_use[j])
                legend(loc=1,fontsize=fs*.75)
            end

            ylabel(L"$\mathrm{Q_w^0}$  $(\mathrm{m^3}$ $\mathrm{s^{-1}})$" ,fontsize=fs)
            tick_params(axis="both" ,labelsize=fs*.9)

            subplot(3,1,2, sharex=ax)
            if time_period == "short"
                plot(tout[ts]/year, -Qbs[ts], linewidth="2", c = colors_to_use[j] )#, label = "Qs sum: $(-Qbs_sum)")
            else
                plot(tout[ts]/year, Images.mapwindow(median!,-Qbs[ts],(11,)), linewidth="2", c = colors_to_use[j] )#, label = "Qs sum: $(-Qbs_sum)")
            end
            ylabel(L"$\mathrm{Q_s^0}$  $(\mathrm{m^3}$ $\mathrm{s^{-1}})$",fontsize=fs)
            tick_params(axis="both" ,labelsize=fs*.9)
            legend()


            subplot(3,1,3,sharex=ax)

            if time_period == "short"
                plot(tout[ts]/year, Images.mapwindow(extrema,-Qbes[ts]*1500 ./ -Qws[ts],(5,)), linewidth="2", c = colors_to_use[j] )#, label = "Qs sum: $(-Qbs_sum)")
                #plot(tout[ts]/year, -Qbes[ts], linewidth="2", c = colors_to_use[j] )#, label = "Qs sum: $(-Qbs_sum)")
                #ylim([0,0.3])
                ylabel(L"$\mathrm{C_{Q_{sc}}^0}$ ($\mathrm{kg}\,\mathrm{m^{-3}}$)",fontsize=fs)
                xlabel("t (years)" ,fontsize=fs)
                tick_params(axis="both" ,labelsize=fs*.9)
                # ax[:yaxis][:set_ticks_position]("right")
                # ax[:yaxis][:set_label_position]("right")
                subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.99,
                                wspace=0.1, hspace=0.04)
            else
                plot(tout[ts]/year,  hts[ts], linewidth="2", c = colors_to_use[j])
                #ylim([0,0.3])
                ylabel(L"$\mathrm{\overline{H_t}}$ ($\mathrm{m}$)",fontsize=fs)
                xlabel("t (years)" ,fontsize=fs)
                tick_params(axis="both" ,labelsize=fs*.9)
                # ax[:yaxis][:set_ticks_position]("right")
                # ax[:yaxis][:set_label_position]("right")
                subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.99,
                                wspace=0.1, hspace=0.04)

            end

            if time_period == "short"
                  savefig("$(fp_save)/$(suites[i])_$(comment).pdf") # saving way too many files... not neat.
            else
                savefig("$(fp_save)/$(suites[i])_$(comment)_long.pdf") # saving way too many files... not neat.
            end

        else
           error("wtf?!?!")
        end

    end
end
