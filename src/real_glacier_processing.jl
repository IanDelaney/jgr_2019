#topography and hydrology input

include("runoff_sort.jl")

if glacier == "gorner"

    ##########
    # Gorner
    ##########
    runoff = readdlm("$(fp)gorner_standard.dat",',',String, skipstart = 1) # read in runoff
    runoff = readdlm("$(fp)gorner_standard_tmp.dat",',',String, skipstart = 1) # read in runoff
    #runoff_spin =  readdlm("$(fp)gornera_GD_h_70_75_spinner.dat",' ',String)
    #runoff =  readdlm("$(fp)gornera_GD_h_69.dat",' ',String)
    #runoff = readdlm("$(fp)gornera_GD_h_2006.dat",' ',String,skipstart=10) # read in runoff

    if  experiment == "hi-res"
        b_0_read_in = readdlm("$(fp)gornera_GD_h_b_0_2014_2017.dat",',',String) # read in runoff
        b_0_spin_read_in = readdlm("$(fp)gornera_GD_h_b_0_2011_2014.dat",',',String) # read in runoff
    elseif experiment == "annual"
        b_0_read_in = readdlm("$(fp)gornera_GD_h_b_0_1972_2017.dat",',',String) # read in runoff
        b_0_spin_read_in = readdlm("$(fp)gornera_GD_h_b_0_69_72.dat",',',String) # read in runoff
    else
        b_0_read_in = readdlm("$(fp)gornera_GD_h_b_0_tester.dat",',',String) # read in runoff
        b_0_spin_read_in = readdlm("$(fp)gornera_GD_h_b_0_tester.dat",',',String) # read in runoff
    end


    bed_dem=read_agr("$(fp)$(glacier)_topo/dem_bed_2007_25m.agr",Float64)
    outline,split_poly=VAWTools.concat_poly(read_xyn("$(fp)$(glacier)_topo/rand_20070913.xyzn",hasz=true,fix=false))
    surf_dem = read_agr("$(fp)$(glacier)_topo/gorner_dem_2007_25m.asc",Float64)

    #########
    # Gries
    #########
elseif glacier == "gries"
    #runoff = readdlm("$(fp)gries_h_2005_2016.dat",',',Float64) # read in runoff Gries
    # runoff_spin = readdlm("$(fp)disall11_final_wm0_88_90_spinner.dat",',',Float64) # read in runoff Gries
    # runoff =     readdlm("$(fp)disall11_final_wm0_2009_2016.csv",',',Float64) # read in runoff Gries #readdlm("$(fp)disall11_final_wm0_2016.csv",',',Float64) # read in runoff Gries
    if experiment == "annual"
        b_0_read_in = readdlm("$(fp)gries_b_0_2009_2016.dat",',',String) # read in runoff
        b_0_spin_read_in = readdlm("$(fp)gries_b_0_2007_2009.dat",',',String) # read in runoff
    else
        b_0_read_in = readdlm("$(fp)gries_b_0_2009_2016.dat",',',String) # read in runoff
        b_0_spin_read_in = readdlm("$(fp)gries_b_0_1991_1994.dat",',',String) # read in runoff
    end

    bed_dem=read_agr("$(fp)$(glacier)_topo/dem_bed_gries.agr",Float64)
    surf_dem = read_agr("$(fp)$(glacier)_topo/gries_dem_2012_25m.agr",Float64)
    outline,split_poly=VAWTools.concat_poly(read_xyn("$(fp)$(glacier)_topo/rand_20120827_subset.xyzn",hasz=true,fix=false))

    #########
    # Aletsch
    #########
elseif glacier == "aletsch"
    # runoff_spin = readdlm("$(fp)massa_blatten_h_tester.dat",',',String)
    # runoff = readdlm("$(fp)aletsch_standard.dat",',',String,skipstart=2) # read in runoff
    # runoff = readdlm("$(fp)massa_blatten_h_2014_2017.dat",',',String) # read in runoff
    if  experiment == "hi-res"
        b_0_read_in = readdlm("$(fp)aletsch_b_0_2014_2017.dat",',',String) # read in runoff
        b_0_spin_read_in = readdlm("$(fp)aletsch_b_0_2011_2014.dat",',',String) # read in runoff
    else
        b_0_read_in = readdlm("$(fp)aletsch_b_0_2014_2017.dat",',',String) # read in runoff
        b_0_spin_read_in = readdlm("$(fp)aletsch_b_0_2011_2014.dat",',',String) # read in runoff
    end


    bed_dem=read_agr("$(fp)$(glacier)_topo/bed_alezg.agr",Float64)
    surf_dem = read_agr("$(fp)$(glacier)_topo/dhm_alezg2009.agr",Float64)
    outline,split_poly=VAWTools.concat_poly(read_xyn("$(fp)$(glacier)_topo/rand_alezg2009.xyzn",hasz=true,fix=false))
end

#########
# Process topography
#########

inpoly_surf=Base.trues(length(surf_dem.x),length(surf_dem.y)) ### might need to sort out x,y,z... as for gries.

for i=1:size(inpoly_surf,1)
    for j=1:size(inpoly_surf,2)
        pt= [surf_dem.x[i],surf_dem.y[j]]
        inpoly_surf[i,j]=inpoly(pt,outline[1:2,:])
    end
end

using Statistics

bands_surf,
bandi_surf,
alphas_surf,areas_surf,
lengths_surf,
widths_surf_,
x_surf,
xmid_surf,
dem_surf, alpha2d_surf=make_1Dglacier(surf_dem,      # gridded data
                                      10.0,          # bins or binsize... issues below 10
                                      inpoly_surf)


bandi_2bed = VAWTools.bandi_for_other_grid(bands_surf, bandi_surf, surf_dem, bed_dem) # translates the surface grid on to the bed grid.
bed_elev = map_onto_bands(bandi_2bed, bed_dem.v, minimum) # creates bed_elevation in to bins determined from surf... We choose minimum as flow likely follows the thalweg.

# Quick and dirty to get around exptraplotation in int_width
xmid_surf_forwidth= zeros(length(xmid_surf)+2)
xmid_surf_forwidth[end]= xmid_surf[end]+(xmid_surf[2]-xmid_surf[1])
xmid_surf_forwidth[2:end-1]= xmid_surf
widths_surf =  zeros(length(widths_surf_)+2)
widths_surf[2:end-1] = widths_surf_


int_width=interpolate((xmid_surf_forwidth,),widths_surf,Interpolations.Gridded(Linear())) #### interpolate width for below

########## Using Dierckx ... has derivative functions...

if glacier == "aletsch"
    spl_surf = Spline1D(xmid_surf,bands_surf,k=3,s=1000.0)
    spl_bed  = Spline1D(xmid_surf,bed_elev  ,k=4,s=7000.0)
elseif glacier == "gorner"
    spl_surf = Spline1D(xmid_surf,bands_surf,k=3,s=2000.0)
    spl_bed  = Spline1D(xmid_surf,bed_elev  ,k=4,s=9000.0)
else
    spl_surf = Spline1D(xmid_surf,bands_surf,k=3,s=1000.0)
    spl_bed  = Spline1D(xmid_surf,bed_elev  ,k=4,s=7000.0)
end

###########################
### process runoff data ###
###########################

# if glacier =="gorner"
#     Q_meas,runoff_date_mod  =  runoffsort_gorner!(runoff)
# end

runoff_date_mod  = runoff_b0_sort!(b_0_read_in)[2]
runoff_date_spin = runoff_b0_sort!(b_0_spin_read_in)[2]

include("interface-real-glacier.jl")

pg = RealGlacier(name=glacier,
                 tspan = (0,5*day),
                 tspan_spin = (runoff_date_spin[1],runoff_date_spin[end]),
                 tspan_run =(runoff_date_mod[1],runoff_date_mod[end]),
                 spl_surf = spl_surf,
                 spl_bed = spl_bed,
                 int_width = int_width
                 )
