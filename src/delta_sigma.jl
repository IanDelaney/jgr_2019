# Bench-like synthetic run
using Parameters
using PyPlot
using DifferentialEquations
import Roots: find_zero
using Dates
using SUGSET
using SUGSET: erosion, post_proc
import SUGSET: zb, zs, source_water, width, source_till, thick, source_water_ddm, Valley, day, year

############################
### parameter definition ###
############################
fp_save ="/Users/idelaney/research/subglacial_sed/sed_erosion_paper/figs/"

access_fac = [1e-4 0.001 0.005 0.01]

figure("scatter",figsize=(9,5))
for i = 1:length(access_fac)

    pg = Valley(tspan=(0, 4.8*year),
                fsl=2.5)

    pg = Valley(pg,
                source_average_time = 1.5*day,
                tout =pg.tspan[1]:day:pg.tspan[2],
                ht0=pg.sgrid[1:end-1] .* 0 .+0.01 # initial till layer thickness
                )

    pp = Phys(Dm=0.04)

    pn = Num(smooth_mobilization=access_fac[i])

    #############################
    ### run model and process ###
    #############################


    println("starting run_model()")
    println(Dates.now())
    @time sol  = run_model(pg, pp, pn)
    println("run_model() finished")

    hts, dht_dts, Qws, Qws_sm, Qbs, Qbes = post_proc(sol,pg, pp, pn)

    println("post_proc() finished")
    println(size(pg.tout), size(Qbs))

    ################
    ### plotting ###
    ################
    fs = 16


    plot(pg.tout[1569:end]./year,abs.(Qbs[1,1569:end]), label = "Δσ: $(access_fac[i])")
    legend(fontsize=fs)
    tick_params(axis="both" ,labelsize=fs)
    xlabel("t (years)",fontsize=fs)
    ylabel(L"Q$^0_s$ (m$^3$ s$^{-1}$",fontsize=fs)

    savefig("$(fp_save)/delta_sigma_e.pdf")
end
