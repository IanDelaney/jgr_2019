
# script to put together grid search for annual runs
using DelimitedFiles
using PyPlot

glacier = ["gorner" "gries"][1]

grid_search = readdlm("/Users/idelaney/research/subglacial_sed/modeling/results/gridsearch/$(glacier)_annual_results_mar18_access0001.txt",'\t', Float64,skipstart=1)

fp_save = "/Users/idelaney/research/subglacial_sed/sed_erosion_paper/figs/"

#########################
### sort out rank cor ###
#########################

if glacier == "gorner"
    base_cor = 0.5
elseif glacier == "gries"
    base_cor = .81
else
    println("wtf")
end

wall_time   = []
stdy_pd_err = []
rank_cor    = []
abs_err     = []
access_fac  = []
dm50        = []
fsl         = []
qsm         = []
spins       = []

for i = 1:length(grid_search[:,1])
    if grid_search[i,2] >= base_cor
        push!(wall_time,    grid_search[i,1])
        push!(rank_cor,    grid_search[i,2])
        push!(abs_err,     grid_search[i,3])
        push!(stdy_pd_err, abs(grid_search[i,4]))
        push!(dm50,  grid_search[i,5])
        push!(fsl,         grid_search[i,6])
        push!(qsm,         grid_search[i,7])
        push!(access_fac,  grid_search[i,8])
        push!(spins,       grid_search[i,9])
    end
end

data = [ stdy_pd_err rank_cor  abs_err dm50  fsl  qsm spins ]
data_ = [ abs_err stdy_pd_err rank_cor dm50 fsl  qsm spins ] # ultra sloppy...
_data_ = [ rank_cor stdy_pd_err  abs_err dm50 fsl  qsm  spins ] # ultra sloppy...

##############################
### grid search processing ###
##############################

## index of best stdy_pd_err

sort_stdy_pd_err = sortslices(data,dims=1, by = x->x[1]) # sort data matrix by date to sort out any issues there!
sort_abs_err= sortslices(data_,dims=1, by = x->x[1]) # sort data matrix by date to sort out any issues there!
sort_rank= sortslices(_data_,dims=1, by = x->x[1]) # sort data matrix by date to sort out any issues there!


println("
        $(glacier)
        number of runs: $(length(grid_search[:,1]))
        number of runs that made cut: $(length(fsl))\n
        RANK
        best rank_cor: $(maximum(rank_cor))... NOTE ignore for gries (4 data points)
        Dm_50: $(sort_rank[1,4])
        fsl: $(sort_rank[1,5])
        smoothing: $(sort_rank[1,6]) \n
        STUDY PERIOD
        smallest study period error: $(sort_stdy_pd_err[1,1])
        Dm_50: $(sort_stdy_pd_err[1,4])
        fsl: $(sort_stdy_pd_err[1,5])
        smoothing: $(sort_stdy_pd_err[1,6]) \n
        ABSOLUTE ERROR
        smallest abs err: $(sort_abs_err[1,1])
        Dm_50: $(sort_abs_err[1,4])
        fsl: $(sort_abs_err[1,5])
        smoothing: $(sort_abs_err[1,6])
                ")

## axis limits
if glacier == "gorner"
    abs_err_limit = [minimum(abs.(abs_err))-0.1*minimum(abs.(abs_err)), 350000]
    err_limit = [0, 350000]
else
    abs_err_limit = [minimum(abs.(abs_err))-0.1*minimum(abs.(abs_err)), maximum(abs.(abs_err))+0.1*maximum(abs.(abs_err))]
    err_limit = [minimum(abs.(stdy_pd_err))-0.1*minimum(abs.(stdy_pd_err)), maximum(abs.(stdy_pd_err))+0.1*maximum(abs.(stdy_pd_err))]

end




## sizes

fs = 15
ms = 40

## parameter grid

figure("parameter-grid", figsize=(10,4))
ax=subplot(1,3,1)
scatter(grid_search[:,8],abs.(grid_search[:,7]),s=ms, c="red", edgecolor="None",zorder=3)
scatter(access_fac,qsm,s=ms, c="blue", edgecolor="None",zorder=3)
ylabel("smoothing period (hrs)",fontsize=fs)
tick_params(axis="both" ,labelsize=fs)
xlabel("access fac. ()",fontsize=fs)
tight_layout()

ax=subplot(1,3,2)
scatter(grid_search[:,7],abs.(grid_search[:,6]),s=ms, c="red", edgecolor="None",zorder=3)
scat = scatter(qsm,fsl,s=ms, c="blue", edgecolor="None",zorder=3)
xlabel("smoothing period  (hrs)",fontsize=fs)
tick_params(axis="both" ,labelsize=fs)
ylabel("sliding fraction",fontsize=fs)
title("$(glacier) parameters")
tight_layout()

ax=subplot(1,3,3)
scatter(grid_search[:,6],abs.(grid_search[:,8]),s=ms, c="red", edgecolor="None",zorder=3)
scat = scatter(fsl,access_fac,s=ms, c="blue", edgecolor="None",zorder=3)
tick_params(axis="both" ,labelsize=fs)
xlabel("sliding fraction",fontsize=fs)
ylabel("access fac ()",fontsize=fs)
tight_layout()

#savefig("$(fp_save)/$(glacier)_annual_grid.pdf") # saving way too many files... not neat.

####
scale = 1000

figure("error", figsize=(5.5,5.5))
ax1=subplot(1,1,1)
p = plot(abs.(data[:,1])./scale,abs.(data[:,3])./scale,"bo")#s=ms,c="red", edgecolor="None",zorder=3)
ylabel(L"$ERR_{\mathrm{sp}}$ (1000 m$^{3}$)", fontsize=fs,color="b")
tick_params(axis="both" ,labelsize=fs)
PyPlot.ylim((abs_err_limit./scale))#abs_err_limit)
PyPlot.xlim((err_limit./scale))
xlabel(L"TERR (1000 m$^{3}$)",fontsize=fs)

ax2 = ax1.twinx()
ax2.spines["right"]
plot(abs.(data[:,1])./scale,abs.(data[:,2]),"rs")#s=ms,c="black", edgecolor="None",zorder=3)
ylabel(L"RANK",fontsize=fs,color="r")
tick_params(axis="both" ,labelsize=fs)
PyPlot.ylim(0, 1.025)
title("$(glacier)gletscher",fontsize=fs*1.25)
tight_layout()

savefig("$(fp_save)/$(glacier)_annual_error.pdf") # saving way too many files... not neat.

## parameter dependency ##

figure("parameter- abs_err", figsize=(8,6))
ax=subplot(3,1,1)
scat = scatter(abs.(data[:,3])./scale,abs.(data[:,6]),s=ms, c="black", edgecolor="None",zorder=3)
ylabel("smoothing period (hrs)",fontsize=fs)
tick_params(axis="both" ,labelsize=fs)
#PyPlot.ylim(0.0, abs_err_limit)
PyPlot.xlim((abs_err_limit./scale))
#xlabel(L"absolute error (m$^{3}$)",fontsize=fs)
title("$(glacier)",fontsize=fs)
tight_layout()

subplot(3,1,2, sharex=ax)
scat = scatter(abs.(data[:,3])./scale,abs.(data[:,5]),s=ms, c="black", edgecolor="None",zorder=3)
ylabel("sliding fraction",fontsize=fs)
tick_params(axis="both" ,labelsize=fs)
#PyPlot.ylim(0.0, abs_err_limit)
PyPlot.xlim((abs_err_limit./scale))
tight_layout()

subplot(3,1,3, sharex=ax)
scat = scatter(abs.(data[:,3])./scale,abs.(data[:,4]),s=ms, c="black", edgecolor="None",zorder=3)
ylabel(L"Dm$_{50}$ (m)",fontsize=fs)
tick_params(axis="both" ,labelsize=fs)
#PyPlot.ylim(0.0, abs_err_limit)
PyPlot.xlim((abs_err_limit./scale))
xlabel(L"$ERR_{\mathrm{sp}}$ (1000 m$^{3}$) ", fontsize=fs)
tight_layout()



#savefig("$(fp_save)/$(glacier)_annual_error_parameters.pdf") # saving way too many files... not neat.
