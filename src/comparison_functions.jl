##################################################
### Parameterized Function for use with DiffEq ###
##################################################
import SUGSET: abs_error, find_sp, NSE, time_step_comp
using StatsBase


####################################
### Optimiztation (w/out DiffEq) ###
####################################

"""
solve_funct(d,n,v)... data,exponent,variables

function to minimize with respect v
"""
function solve_funct(v,data,fp_save,glacier,pp,pg,pn)
# maw: where does v[4] feature?
    #global count
    count[1] += 1
    println("$(glacier) \n runs: $(count)")
    println("varies Dm: ",v[1],"  varies fi: ",v[2],"   varies fsl: ",v[3], "   varies Q_sm: ",v[4])


    pp = Phys(
              Dm = v[1],
              ft = v[2],
              fi = v[2],
              )


    if glacier == "synth-bench"
        pg = Valley(
                    tspan=(0,4*year),
                    fsl = v[3],
                    #int_b_0_sm = interpolate((runoff_date_mod,),boxcar(b_0_,Int( v[4])), Interpolations.Gridded(Constant()))
                    )
    else
        pg = RealGlacier(
                         tspan=(runoff_date_mod[1],runoff_date_mod[end]),
                         fsl = v[3],
                         int_b_0_sm = interpolate((runoff_date_mod,),VAWTools.boxcar(b_0_,(Int(v[4]),0),ones(length(b_0_))), Interpolations.Gridded(Constant()))
                         )

    end

    begin_sp,end_sp =SUGSET.find_sp(global_date,pg)

    out = 0.0 # is this needed

    try
        sol  =  run_model(pg, pp, pn) # run model

        # post-proc between here and...
        hts  =  sol(pg.tout)
        Qbs  =  zeros(length(pg.sgrid), length(pg.tout))
        # Qbes =  zeros(length(pg.sgrid), length(pg.tout))
        # dht_dts = zeros(length(pg.sgrid)-1, length(pg.tout))

        for i in eachindex(pg.tout)
            t = pg.tout[i]
            Qbs[:,i] = dht_dt_fn(hts[i], pg.sgrid, t, pg, pp, pn)[2]
        end
        # ... here

        # cost function
        out =  abs(sum(-(Qbs[1,begin_sp:end_sp-1])-data[1:end-1]))

    catch
        out = Inf
    end

    open("$(fp_save)/$(glacier)_optim_results.txt", "a") do f
        write(f, "\n$(now());varies Dm $(v[1]);varies fi $(v[2]);varies fsl$(v[3]);varies Q_sm $(v[4]); error $(out); runs $(count);")
    end

    println("error:",out)

    return out

end

############
### Grid ###
############
"""
comparerer(data,global_date, pg,pp,pn)
    compares model output to data
"""
function  comparerer(data,global_date, pg,pp,pn)

    pg = RealGlacier(pg,
                     spin = true, # set spin == to true to call proper water functions
                     tspan=pg.tspan_spin
                     )

    pg = RealGlacier(pg,
                     tout=pg.tspan[1]:.2*SUGSET.day:pg.tspan[2] # output times... MAKE SURE IT IS THE SAME AS FOR REAL_GLACIER PROCESSING.
                     )

    # spin up
    println("beginning spin")
    hts_evol,runs = SUGSET.spinner(RealGlacier,pg,pp,pn,0.75e-3)
    println(mean(hts_evol))

    # pass in hts to pg
    pg = RealGlacier(pg,
                     spin = false,
                     ht0=hts_evol,# initial till layer thickness changed to spinner output
                     tspan=pg.tspan_run
                     )

    println(mean(pg.ht0))
    pg = RealGlacier(pg,
                     tout=pg.tspan[1]:.2*SUGSET.day:pg.tspan[2] # output times
                     )

    #    run model with it all
    sol  =  run_model(pg, pp, pn) # run model

    # post-proc between here and...
    Qbs = SUGSET.Qbs_post_proc(sol,pg,pp,pn)

    # ... here

    begin_sp,end_sp =SUGSET.find_sp(global_date,pg)
    Qbs_sum = sum(abs.(Qbs[begin_sp:end_sp-1]))

    # 0.2 day
    model_02day, data_02day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                 abs.(data[1:end-1]),
                                 0.2*SUGSET.day,
                                 pg.tout[begin_sp:end_sp-1])

    err_02day = abs_error(model_02day, data_02day)
    nse_02day = NSE(model_02day[1:end-1], data_02day[1:end-1])

    # 0.25 day
    model_025day, data_025day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                               abs.(data[1:end-1]),
                                               0.25*SUGSET.day,
                                               pg.tout[begin_sp:end_sp-1])

    err_025day = abs_error(model_025day, data_025day)
    nse_025day = NSE(model_025day[1:end-1], data_025day[1:end-1])

    # 0.3 day
    model_03day, data_03day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                             abs.(data[1:end-1]),
                                             0.3*SUGSET.day,
                                             pg.tout[begin_sp:end_sp-1])

    err_03day = abs_error(model_03day, data_03day)
    nse_03day = NSE(model_03day[1:end-1], data_03day[1:end-1])

    # 0.4 day
    model_04day, data_04day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                             abs.(data[1:end-1]),
                                             0.4*SUGSET.day,
                                             pg.tout[begin_sp:end_sp-1])

    err_04day = abs_error(model_04day, data_04day)
    nse_04day = NSE(model_04day[1:end-1], data_04day[1:end-1])

    # 0.5 day
    model_05day, data_05day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                             abs.(data[1:end-1]),
                                             0.5*SUGSET.day,
                                             pg.tout[begin_sp:end_sp-1])

    err_05day = abs_error(model_05day, data_05day)
    nse_05day = NSE(model_05day[1:end-1], data_05day[1:end-1])

    # 0.75 day
    model_075day, data_075day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                               abs.(data[1:end-1]),
                                               0.75*SUGSET.day,
                                               pg.tout[begin_sp:end_sp-1])

    err_075day = abs_error(model_075day, data_075day)
    nse_075day = NSE(model_075day[1:end-1], data_075day[1:end-1])

    # 1 day
    model_day, data_day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                         abs.(data[1:end-1]),
                                         SUGSET.day,
                                         pg.tout[begin_sp:end_sp-1])

    err_day = abs_error(model_day, data_day)
    nse_day = NSE(model_day[1:end-1], data_day[1:end-1])

    # 1.25 day
    model_125day, data_125day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                         abs.(data[1:end-1]),
                                         1.25*SUGSET.day,
                                         pg.tout[begin_sp:end_sp-1])

    err_125day = abs_error(model_125day, data_125day)
    nse_125day = NSE(model_125day[1:end-1], data_125day[1:end-1])

    # 1.5 day
    model_150day, data_150day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                         abs.(data[1:end-1]),
                                         1.5*SUGSET.day,
                                         pg.tout[begin_sp:end_sp-1])

    err_150day = abs_error(model_150day, data_150day)
    nse_150day = NSE(model_150day[1:end-1], data_150day[1:end-1])

    # 2 day
    model_2day, data_2day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                         abs.(data[1:end-1]),
                                         2*SUGSET.day,
                                         pg.tout[begin_sp:end_sp-1])

    err_2day = abs_error(model_2day, data_2day)
    nse_2day = NSE(model_2day[1:end-1], data_2day[1:end-1])


    # 2.5 day
    model_250day, data_250day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                               abs.(data[1:end-1]),
                                               2.5*SUGSET.day,
                                               pg.tout[begin_sp:end_sp-1])

    err_250day = abs_error(model_250day, data_250day)
    nse_250day = NSE(model_250day[1:end-1], data_250day[1:end-1])

    # 3 day
    model_3day, data_3day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                               abs.(data[1:end-1]),
                                               3*SUGSET.day,
                                               pg.tout[begin_sp:end_sp-1])

    err_3day = abs_error(model_3day, data_3day)
    nse_3day = NSE(model_3day[1:end-1], data_3day[1:end-1])

    # 5 day
    model_5day, data_5day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                               abs.(data[1:end-1]),
                                               5*SUGSET.day,
                                               pg.tout[begin_sp:end_sp-1])

    err_5day = abs_error(model_5day, data_5day)
    nse_5day = NSE(model_5day[1:end-1], data_5day[1:end-1])

    # 7 day
    model_7day, data_7day = time_step_comp(abs.(Qbs[begin_sp:end_sp-1]),
                                               abs.(data[1:end-1]),
                                               7*SUGSET.day,
                                               pg.tout[begin_sp:end_sp-1])

    err_7day = abs_error(model_7day, data_7day)
    nse_7day = NSE(model_7day[1:end-1], data_7day[1:end-1])


    println("\n sum Qbs:",sum(abs.(Qbs[begin_sp:end_sp-1])),
            "\n sum data:",sum(abs.(data[1:end-1]))
            )

    println("comparerer success!!")
    return  err_02day, err_025day,  err_03day, err_04day, err_05day,  err_075day, err_day, err_125day, err_2day, err_250day, err_3day, err_5day, err_7day, nse_02day, nse_025day,  nse_03day, nse_04day, nse_05day,  nse_075day, nse_day, nse_125day, nse_2day, nse_250day, nse_3day, nse_5day, nse_7day, Qbs_sum, sum(abs.(data[1:end-1])), runs
end



"""
    data_sums(data,model,pg)

    sums model output to compare to data sums (i.e. on annual scale)

    data should be of the format of gries_annual.jl
"""
function data_sums(data,model,pg) # finds sums of data for output quantities
    x = 0

    Qs_bracket_model =zeros(length(data[:,3]))
 #   time_bracket_model =zeros(length(data[:,3]),2)


    for j = 1:length(Qs_bracket_model)
        #println(j)
        out_model=[]
        time_model=[]

        for i=1:length(model)
           # println(i)
            if pg.tout[i] >= data[j,1]+x && pg.tout[i] <= data[j,2]+x
                push!(out_model,abs(model[i]))
#                push!(time_model, pg.tout[i])
            end
        end

  #      time_bracket_model[j,1] = minimum(time_model)
   #     time_bracket_model[j,2] = maximum(time_model)
        Qs_bracket_model[j]   = sum(out_model).*(pg.tout[2]-pg.tout[1])
    end

    return Qs_bracket_model#, time_bracket_model
end

"""
    comparerer_annual(data,pg,pp,pn)

similar to comparer... but for annual measurements
"""
function comparerer_annual(data,pg,pp,pn)
    pg = RealGlacier(pg,
                     spin = true, # set spin == to true to call proper water functions
                     tspan=pg.tspan_spin
                     )

    pg = RealGlacier(pg,
                     tout=pg.tspan[1]:.2*day:pg.tspan[2] # output times
                     )

    #spin up
    println("beginning spin")
    hts_evol, runs = SUGSET.spinner(RealGlacier,pg,pp,pn,0.75e-3)

    # # pass in hts to pg
    pg = RealGlacier(pg,
                     spin = false,
                     ht0=hts_evol,
                     tspan=pg.tspan_run) # initial till layer thickness


    pg = RealGlacier(pg,
                     tout=pg.tspan[1]:.2*day:pg.tspan[2] # output times
                     )

    sol  =  run_model(pg, pp, pn) # run model
    # post_proc...
    Qbs = SUGSET.Qbs_post_proc(sol,pg,pp,pn)
    # ... here

    summed_model_out = data_sums(data,Qbs,pg)

    err = sum(data[:,3])-sum(summed_model_out)

    abs_err = SUGSET.abs_error(data[:,3],summed_model_out)

    rank_cor = corspearman(summed_model_out,data[:,3])

    return err, rank_cor, abs_err, runs
end
