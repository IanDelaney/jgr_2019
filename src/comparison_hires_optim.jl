#############
# script to compare model runs to real data...

using Dierckx
using DelimitedFiles
using VAWTools
using Parameters
using Interpolations
using Dates
using SmoothingSplines
using ParameterizedFunctions
using Distributions
import Roots: find_zero
using SUGSET
using SUGSET: erosion, post_proc, solvers
import SUGSET: zb, zs, gradzb, gradzs, source_water, width, source_till, thick, find_sp,b_0_determ, spinner, source_water_ddm,dht_dt_fn, Q_w, NSE, NSE_log, day, year

include("comparison_functions.jl")

experiment = "hi-res"
glacier=["gorner", "gries", "fiescher", "aletsch"][1]
hydro_type = ["steady","ddm","overdeepening", "future_evolution","real"][5] # only used for synthetic runs... usuall use ddm

scheme = ["Grid", "ParamFunc", "Compare" ][end] # "Grid" does grid search. "Optim" uses optimization routine. "Compare" does a single run

println("\n site: $(glacier) \n method: $(scheme)")

if homedir()=="/home/mauro"
    fp="/home/mauro/netfs/vaw/root"
    fp_save = "../../tmp/"
elseif homedir()== "/Users/ian"
    fp="/Users/ian/data/sed_modeling_data/$(glacier)/" # file path to data on Ian's computer
    fp_save = "/Users/ian/eth/subglacial_sed/modeling/results/"
elseif homedir()== "/Users/idelaney"

    fp="/Users/idelaney/data/sed_modeling_data/$(glacier)/" # file path to data on Ian's JPL computere
    fp_save = "/Users/idelaney/research/subglacial_sed/modeling/results/"
elseif homedir()== "/home/idelaney"
    fp="/home/idelaney/data/sed_modeling_data/$(glacier)/" # file path to data on Ian's computer
    fp_save = "/home/idelaney/sed_transport/model_outputs/"

elseif homedir()== "/Users/idelaney"

    fp="/Users/idelaney/data/sed_modeling_data/$(glacier)/" # file path to data on Ian's JPL computere
    fp_save = "/Users/idelaney/research/subglacial_sed/modeling/results/"

elseif homedir()=="/home/delaneyi"

    fp="/scratch_net/esker/delaneyi/sed_modeling_data/$(glacier)/" # file path to data on scratch disk
    fp_save = "/home/delaneyi/analysis/sed_modeling_outputs/"
end

include("real_glacier_processing.jl")

save_run = [true false][1]
bedload = [true false][1]

#######################
### setup and paras ###
#######################

#for synth
if glacier == "gorner"

    varies =[0.03225 #Dm... grain size
             1.2 #sliding fraction
	     5.0#smoothing factor
             0.0001#access fac
             ]

elseif glacier == "aletsch"
    varies =[0.03225 #Dm... grain size
             0.85 #sliding fraction...
            102 #smoothing factor
	      ]
end


lower = [0.0025#Dm... grain size
         0.5 #sliding fraction...
         5 	      #smoothing factor
	 0         #access_fac
         ]

upper = [0.3 #Dm... grain size
         2.25 #sliding fraction...
         24*7 #smoothing factor (hrs)
	 1 #access_fac
         ]

pn = Num()


# this flow could be a little buggy

pp = Phys(Dm = varies[1]
          )

pg = RealGlacier(name = glacier,
                 tspan_run=(runoff_date_mod[1],runoff_date_mod[end]),
                 tspan=(runoff_date_mod[1],runoff_date_mod[end]),
                 fsl = varies[2],
                 tspan_spin = (runoff_date_spin[1],runoff_date_spin[end]),
                 spl_surf = spl_surf,
                 spl_bed = spl_bed,
                 int_width = int_width
                 )

############
### data ###
############
### read in data ###

if glacier == "gorner"
    data   = readdlm("$(fp)Gornera_calib.txt",';',Float64,skipstart=2) # read instrument files
    data = abs.(sortslices(data, dims=1, by=x->(x[1]))) # sort data matrix by date to sort out any issues there!

elseif glacier == "aletsch"
    data   = readdlm("$(fp)Massa_calib.txt",';',Float64,skipstart=2) # read instrument files
    data = abs.(sortslices(data, dims=1, by=x->(x[1]))) # sort data matrix by date to sort out any issues there!
end

### sort variables
if glacier == "gorner"
    global_date = data[:,1]
    sedflux_proper = data[:,4]
    discharge_ts = data[:,5]
elseif glacier == "aletsch"
    global_date = data[:,1]
    sedflux_proper = data[:,4]
    discharge_ts = data[:,5]
end

begin_sp, end_sp = find_sp(global_date,pg)

sedflux_proper_ = boxcar(sedflux_proper,5) # transfered to volume and change to m3/s, second number is factor by which to smooth ... maybe not as important now.

knots = (global_date,)
itp_flux = interpolate(knots, sedflux_proper_, Interpolations.Gridded(Linear()))
itp_discharge = interpolate(knots, discharge_ts, Interpolations.Gridded(Linear()))

sedflux_rs_  =  itp_flux(pg.tout[begin_sp:end_sp])
discharge_rs_   =  itp_discharge(pg.tout[begin_sp:end_sp])

sedflux_rs = sedflux_rs_

for i = 1:length(sedflux_rs_)
    if sedflux_rs_[i] == NaN
        sedflux_rs[i] = 0
    end
end

# ##############################
# ### Take care of hydrology ###
# ##############################


pg = RealGlacier(pg,
                 spin=true,
                 int_b_0_spin =  Spline1D(runoff_date_spin,pg.b_0_spin; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11),
                 int_b_0_sm_spin = Spline1D(runoff_date_spin,
                                            Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                              pg.b_0_spin,
                                                              (Int(div(pg.Q_sm,2)*2+1),)
                                                              )
                                            ; w= ones(length(runoff_date_spin)), k=2, bc="nearest", s=1.2e-11
                                            ))

pg = RealGlacier(pg,
                 tspan=pg.tspan_spin,
                 int_b_0    = Spline1D(runoff_date_mod,pg.b_0; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11),
                 int_b_0_sm =  Spline1D(runoff_date_mod,
                                        Images.mapwindow( v->quantile(v, pg.source_quantile),
                                                              pg.b_0,
                                                              (Int(div(pg.Q_sm,2)*2+1),) # make odd
                                                              )
                                        ; w= ones(length(runoff_date_mod)), k=2, bc="nearest", s=1.2e-11
                                        )
                 )

####################
### Optimization ###
####################

# Bounds for solver

if scheme == "Grid"
    include("grid_search_pmap.jl")
   walltimes, vari_1, vari_2, vari_3, vari_4, err_02day, err_025day,  err_03day, err_04day, err_05day, err_075day,
            err_day, err_125day, err_2day, err_250day, err_3day, err_5day, err_7day, nse_02day, nse_025day,  nse_03day,
            nse_04day, nse_05day,  nse_075day, nse_day, nse_125day, nse_2day, nse_250day, nse_3day, nse_5day, nse_7day, Qbs_sum, data_sum, runs = grid_search_hires(pp, pg, pn, sedflux_rs, global_date, runoff_date_spin, runoff_date_mod, fp_save)


elseif scheme == "ParamFunc"

    prob = make_problem(varies, pg, pp, pn)
    cost_function = build_loss_objective(prob,
                                         pn.alg,
                                         make_loss_Qbs(global_date,sedflux_rs))

    result = optimize(cost_function,
                      varies,
                      Optim.Options(show_trace = true,
                                    show_every = 1,
                                    iterations = 1000
                                    )
                      )

elseif scheme == "Compare"

    pg = RealGlacier(pg,
                     spin = true, # set spin == to true to call proper water functions
                     tspan=pg.tspan_spin
                     )

    pg = RealGlacier(pg,
                     tout=pg.tspan[1]:.2*SUGSET.day:pg.tspan[2] # output times... MAKE SURE IT IS THE SAME AS FOR REAL_GLACIER PROCESSING.
                     )
    pn = Num(smooth_mobilization=varies[4])

    # spin up

    hts_evol,spins = spinner(RealGlacier,pg,pp,pn,0.75e-3)

    # pass in hts to pg
    pg = RealGlacier(pg,
                     spin = false,
                     ht0=hts_evol,# initial till layer thickness changed to spinner output
                     tspan=pg.tspan_run
                     )

    println(mean(pg.ht0))
    pg = RealGlacier(pg,
                     tout=pg.tspan[1]:.2*SUGSET.day:pg.tspan[2] # output times
                     )

    #    run model with it all
    sol  =  run_model(pg, pp, pn) # run model


    hts, dht_dts, Qws, Qws_sm, Qbs, Qbes = post_proc(sol,pg, pp, pn)

    @unpack sgrid, ht0, tspan, tout = pg

    zs_= []
    zb_=[]
    for j = 1:length(pg.sgrid)
        push!(zs_, zs(pg.sgrid[j],pg.tout[1],pg))
        push!(zb_, zb(pg.sgrid[j],pg.tout[1],pg))
    end


    #verbose && (println("post-proc start"); tic())
        # post-proc
    hts     =  sol(tout)
    Qws     =  zeros(length(sgrid), length(tout))
        Qws_sm  =  zeros(length(sgrid), length(tout))
    Qbs     =  zeros(length(sgrid), length(tout))
    #  dphi_dx =  zeros(length(sgrid), length(tout))
    Qbes    =  zeros(length(sgrid), length(tout))
    dht_dts =  zeros(length(sgrid)-1, length(tout))

    for i in eachindex(tout)
        t = tout[i]
        #     dphi_dx = SUGSET.dphidx(sgrid, t, pg, pp)
        Qws[:,i] = SUGSET.Q_w(sgrid, t, pg, pp)
        Qws_sm[:,i] = SUGSET.Q_w_sm(sgrid, t, pg, pp)
        dht_dts[:,i],Qbs[:,i],Qbes[:,i] = SUGSET.dht_dt_fn(hts[i], sgrid, t, pg, pp, pn)
    end

    hts = hcat(hts...)
    #verbose && (println("post-proc end"); toc())


    smid=zeros(length(pg.sgrid)-1)

    for i =length(pg.sgrid)-1:-1:1
        smid[i] = (pg.sgrid[i+1] + sgrid[i])/2 #non stagard grid
    end

    w = zeros(length(smid))
    for i =1:length(smid)
        w[i]= width(smid[i],0,pg)
    end

    source_year =   SUGSET.erosion.(smid, Ref(0.0), Ref(pg), Ref(pp))*year




    # ... here
    hts = sol(pg.tout)

    if save_run == true

        using JLD


         ###########################
        ### save test variables ###
        ###########################


        save("$(fp_save)$(glacier)_hi-res_$(varies[1])_$(varies[2])_$(varies[3])_$(varies[4]).jld",
             "zs",    zs_, #geometry
             "zb",    zb_,
             "width", w,
             "begin_sp", begin_sp,
             "end_sp", end_sp,
             "source_year", source_year,

             "tout",       pg.tout, #parameters
             "sgrid",      pg.sgrid,
             "ht0",        pg.ht0,
             "fsl",        pg.fsl,
             "Dm",         pp.Dm,
             "lr",         pp.lr,
             "fi",         pp.fi,
             "Qws",   Qws, #outputs
             "sedflux_rs", sedflux_rs,
             #"dphi_dx", dphi_dx,
             "Qws_sm",Qws_sm,
             "Qbes",  Qbes,
             "hts",   hts,
             "Qbs",   Qbs,
             )
    end

else
    error("WTF??!?!?!")
end
